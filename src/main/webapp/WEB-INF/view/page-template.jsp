<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="/tester/static/css/bootstrap.css">
		<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">-->

		<title>Tester</title>
	</head>

	<body>
		<c:if test="${ERROR!=null}">
			<header>Error: ${ERROR}</header>
		</c:if>
		
		<div class="container">
			<div class="row">
				<jsp:include page="${currentPage}"/>
				<c:if test="${CURRENT_ACCOUNT!=null}">
					<jsp:include page="navigation_menu.jsp"/>
				</c:if>
				
			</div>
		</div>
		<footer></footer>
		<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"/>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"/>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"/>-->

		<script src="/tester/static/js/popper.js"/>
		<script src="/tester/static/js/jquery-3.3.1.js"/>
		<script src="/tester/static/js/bootstrap.js"/>
	</body>
</html>
