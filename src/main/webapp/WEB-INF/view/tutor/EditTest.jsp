﻿<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<div class="col-sm-7">
	<div class="card">
		<h5 class="card-header text-success">Edit test form</h5>
		<div class="card-body">
			<form action="/tester/tutor/edit-test?IdTest=${TEST.id }" method="post">		
				<div class="form-group row .bg-light">
					<label for="inputLogin" class="col-sm-3 col-form-label">
						Test name
					</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="testName" name="TEST_NAME" placeholder=${TEST.testName}>
					</div>
				</div>
				<div class="form-group row">
					<label for="inputDescription" class="col-sm-3 col-form-label">
						Description
					</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="inputDescription" name="TEST_DESCRIPTION" placeholder=${TEST.description}>
						</div>
				</div>
				<div class="form-group row">
					<label for="inputDuration" class="col-sm-3 col-form-label">
						Duration per question
					</label>
					<div class="col-sm-9">
						<input class="form-control" id="inputDuration" name="TEST_DURATION" placeholder=${TEST.duration_per_question}>
						</div>
				</div>
			

				<div class="row">
					<div class="col-sm-6">
						<div class="row justify-content-start form-group">
							<button type="submit" class="btn btn-primary" >Save changes</button>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="row justify-content-end">
							<a href="/tester/tutor/edit-question-list?IdTest=${TEST.id}" class="btn btn-warning" >Edit questions</a>
						</div>

					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="row justify-content-center">
							<a href="/tester/tutor/edit-test-list" type="button" class="btn btn-secondary">Back to test list</a>
						</div>
					</div>
				</div>
				
			</form>
		</div>	

	</div>
</div>

									