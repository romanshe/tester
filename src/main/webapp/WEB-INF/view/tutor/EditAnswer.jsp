﻿<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<div class="col-sm-7">
	<div class="card">
		<h5 class="card-header text-success">Edit answer form</h5>
		<div class="card-body">
			<form action="/tester/tutor/edit-answer?IdTest=${IdTest}&QUESTION_ID=${QUESTION_ID}&ANSWER_ID=${ANSWER.id}" method="post">		
				<div class="form-group row .bg-light">
					<label for="inputAnswer" class="col-sm-3 col-form-label">
						Answer
					</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="answerName" name="ANSWER_NAME" value="${ANSWER.name}">
					</div>
				</div>
				<input type="hidden" class="form-control" id="inputIdAnswer" name="ANSWER_ID_QUESTION" value="${QUESTION_ID}">
				<div class="form-group row">
					<label for="inputAnswerCorrect" class="col-sm-3 col-form-label">
						Correct?
					</label>
					<div class="col-sm-9">
						<input class="form-check-input" type="checkbox" id="inputAnswerCorrect" name="ANSWER_CORRECT" <c:if test="${ANSWER.correct}">checked</c:if>>
					</div>
				</div>		

				<div class="form-group row">
					<div class="col-sm-6">
						<div class="row justify-content-start form-group">
							<button type="submit" class="btn btn-primary">Save changes</button>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="row justify-content-center">
							<a href="/tester/tutor/edit-answer-list?IdTest=${IdTest}&QUESTION_ID=${QUESTION_ID}" type="button" class="btn btn-secondary">Back to answer list</a>
						</div>
					</div>
				</div>
				
			</form>
		</div>	

	</div>
</div>

									