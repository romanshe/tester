﻿<%@ page contentType="text/html; UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<div class="col-sm-10">
	<div class="card">
		<h5 class="card-header text-dark text-center">Answer list</h5>
		<div class="card-body">
			<table class="table">
			  <thead class="thead-dark">
				<tr>
				  <th scope="col">#</th>
				  <th scope="col">Answer</th>
				  <th scope="col">Correct</th>
				  <th scope="col">Action</th>
				</tr>
			  </thead>
			  <tbody>
				<c:forEach var="answer" items="${ANSWER_LIST}">
					<tr>
					  <th scope="row">${answer.id}</th>
					  <td>${answer.name}</td>
					  <td>${answer.correct}</td>
					  <td>
							<a href="/tester/tutor/edit-answer?QUESTION_ID=${answer.id_question}&IdTest=${IdTest}&ANSWER_ID=${answer.id}" type="button" class="btn btn-warning">Edit</a>
							<a href="/tester/tutor/remove-answer?QUESTION_ID=${answer.id_question}&IdTest=${IdTest}&ANSWER_ID=${answer.id}" type="button" class="btn btn-danger">Remove</a>
					  </td>
					</tr>
				</c:forEach>
			  </tbody>
			</table>
			<div class="row">
				<div class="col-sm-6">
					<div class="row justify-content-center">
						<a href="/tester/tutor/add-answer?IdTest=${IdTest}&QUESTION_ID=${QUESTION_ID}" type="button" class="btn btn-primary">Add answer</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="row justify-content-center">
						<a href="/tester/tutor/edit-question?IdTest=${IdTest}&QUESTION_ID=${QUESTION_ID}" type="button" class="btn btn-secondary">Back to edit question</a>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>




									