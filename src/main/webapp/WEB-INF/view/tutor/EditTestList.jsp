﻿<%@ page contentType="text/html; UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<div class="col-sm-10">
	<div class="card">
		<h5 class="card-header text-dark text-center">Test list</h5>
		<div class="card-body">
			<table class="table">
			  <thead class="thead-dark">
				<tr class="row">
				  <th class="col-1">#</th>
				  <th class="col-2">Name</th>
				  <th class="col-2">Description</th>
				  <th class="col-2">Duration</th>
				  <th class="col-2">Author</th>
				  <th class="col-3">Action</th>
				</tr>
			  </thead>
			  <tbody>
				<c:forEach var="test" items="${TEST_LIST}">
					<tr class="row">
					  <td class="col-1">${test.id}</td>
					  <td class="col-2" >${test.testName}</td>
					  <td class="col-2" >${test.description}</td>
					  <td class="col-2" >${test.duration_per_question}</td>
					  <td class="col-2" >${test.authorName}</td>
					  <td class="col-3">
							<a href="/tester/tutor/edit-test?IdTest=${test.id }" type="button" class="btn btn-warning">Edit</a>
							<a href="/tester/tutor/remove-test?IdTest=${test.id }" type="button" class="btn btn-danger">Remove</a>
					  </td>
					</tr>
				</c:forEach>
			  </tbody>
			</table>
			<div class="row">
				<div class="col-sm-12">
					<div class="row justify-content-center">
						<a href="/tester/tutor/add-test" type="button" class="btn btn-primary">Add test</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




									