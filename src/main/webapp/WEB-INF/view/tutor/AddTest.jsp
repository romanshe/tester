﻿<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<div class="col-sm-7">
	<div class="card">
		<h5 class="card-header text-success">Add test form</h5>
		<div class="card-body">
			<form action="/tester/tutor/add-test" method="post">		
				<div class="form-group row .bg-light">
					<label for="inputLogin" class="col-sm-3 col-form-label">
						Test name
					</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="testName" name="TEST_NAME" placeholder="Test name">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputDescription" class="col-sm-3 col-form-label">
						Description
					</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="inputDescription" name="TEST_DESCRIPTION" placeholder="Description">
						</div>
				</div>
				<div class="form-group row">
					<label for="inputDuration" class="col-sm-3 col-form-label">
						Duration per question
					</label>
					<div class="col-sm-9">
						<input class="form-control" id="inputDuration" name="TEST_DURATION" placeholder="Duration per question">
						</div>
				</div>
			

				<div class="form-group row">
					<div class="col-sm-6">
						<button type="submit" class="btn btn-primary" >Add test</button>
					</div>
				</div>
			</form>
		</div>	

	</div>
</div>

									