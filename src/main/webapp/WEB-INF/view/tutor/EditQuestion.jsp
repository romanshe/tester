﻿<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<div class="col-sm-7">
	<div class="card">
		<h5 class="card-header text-success">Edit question form</h5>
		<div class="card-body">
			<form action="/tester/tutor/edit-question?IdTest=${IdTest}&QUESTION_ID=${QUESTION_ID}" method="post">		
				<div class="form-group row .bg-light">
					<label for="inputQuestion" class="col-sm-3 col-form-label">
						Question
					</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="inputQuestion" name="QUESTION_NAME" value="${QUESTION.question}">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-6">
						<div class="row justify-content-start form-group">
							<button type="submit" class="btn btn-primary">Save changes</button>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="row justify-content-end">
							<a href="/tester/tutor/edit-answer-list?QUESTION_ID=${QUESTION.id}&IdTest=${IdTest}" class="btn btn-warning" >Edit answers</a>
						</div>
					</div>
				</div>		
				
				<div class="row">
					<div class="col-sm-12">
						<div class="row justify-content-center">
							<a type="button" class="btn btn-secondary" href="/tester/tutor/edit-question-list?IdTest=${IdTest}&QUESTION_ID=${QUESTION.id }">Back to question list</a>
						</div>
					</div>
				</div>
				
			</form>
		</div>	

	</div>
</div>

									