﻿<%@ page contentType="text/html; UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<div class="col-sm-10">
	<div class="card">
		<h5 class="card-header text-dark text-center">Question list</h5>
		<div class="card-body">
			<table class="table">
			  <thead class="thead-dark">
				<tr>
				  <th scope="col" >#</th>
				  <th scope="col" >Question</th>
				  <th scope="col" >Action</th>
				</tr>
			  </thead>
			  <tbody>
				<c:forEach var="question" items="${QUESTION_LIST}">
					<tr>
					  <th scope="row">${question.id}</th>
					  <td>${question.question}</td>
					  <td>
							<a href="/tester/tutor/edit-question?QUESTION_ID=${question.id }&IdTest=${IdTest}" type="button" class="btn btn-warning">Edit</a>
							<a href="/tester/tutor/remove-question?QUESTION_ID=${question.id}&IdTest=${IdTest}" type="button" class="btn btn-danger">Remove</a>
					  </td>
					</tr>
				</c:forEach>
			  </tbody>
			</table>
			<div class="row">
				<div class="col-sm-6">
					<div class="row justify-content-center">
						<a href="/tester/tutor/add-question?IdTest=${IdTest}" type="button" class="btn btn-primary">Add question</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="row justify-content-center">
						<a href="/tester/tutor/edit-test?IdTest=${IdTest}" type="button" class="btn btn-secondary">Back to edit test</a>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>




									