
<div class="col-sm-10">
	<div class="card">
		<h5 class="card-header text-success">Sign Up form</h5>
		<div class="card-body">
			<form action="/tester/sign-up" method="post">
				<div class="form-group row .bg-light">
					<label for="inputLogin" class="col-sm-3 col-form-label">
						Login
					</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="inputLogin" name="LOGIN" placeholder="Login">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword" class="col-sm-3 col-form-label">
						Password
					</label>
					<div class="col-sm-9">
						<input type="password" class="form-control" id="inputPassword" name="PASSWORD" placeholder="Password">
						</div>
				</div>
				<div class="form-group row">
					<label for="inputPasswordConfirmation" class="col-sm-3 col-form-label">
						Password Confirmation
					</label>
					<div class="col-sm-9">
						<input type="password" class="form-control" id="inputPasswordConfirmation" placeholder="Password Confirmation">
						</div>
				</div>
				<div class="form-group row">
					<label for="inputFirstName" class="col-sm-3 col-form-label">
						First Name
					</label>
					<div class="col-sm-9">
						<input class="form-control" id="inputFirstName" name="FIRST_NAME" placeholder="First name">
						</div>
				</div>
				<div class="form-group row">
					<label for="inputLastName" class="col-sm-3 col-form-label">
						Last Name
					</label>
					<div class="col-sm-9">
						<input class="form-control" id="inputLastName" name="LAST_NAME" placeholder="Last name">
						</div>
				</div>
				<div class="form-group row">
					<label for="inputMiddleName" class="col-sm-3 col-form-label">
						Middle Name
					</label>
					<div class="col-sm-9">
						<input class="form-control" id="inputMiddleName" name="MIDDLE_NAME" placeholder="Middle name">
						</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail" class="col-sm-3 col-form-label">
						Email
					</label>
					<div class="col-sm-9">
						<input class="form-control" id="inputEmail" name="EMAIL" placeholder="Email">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputRole" class="col-sm-3 col-form-label">
						Initial role
					</label>
					<div class="col-sm-9">
						<label for="inputRole" class="col-form-label form-control">
							Student
						</label>
					</div>
				</div>

				<div class="form-group row">
					<div class="col-sm-4">
						<button type="submit" class="btn btn-primary"  >Sign Up</button>
					</div>
					<div class="col-sm-4">
						<a  class="btn btn-secondary" href="/tester/login" method="get">Back to Sign In</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>



