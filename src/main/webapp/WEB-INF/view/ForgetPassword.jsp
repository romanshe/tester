
<div class="col-sm-7">
	<div class="card">
		<h5 class="card-header text-success">Forget password form</h5>
		<div class="card-body">
			<form action="login" method="post">		

				<div class="form-group row">
					<label for="inputEmail" class="col-sm-3 col-form-label">
						Email
					</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="inputEmail" name="Email" placeholder="Email">
						</div>
				</div>
				

				<div class="form-group row">
					<div class="col-sm-4">
						<button type="submit" class="btn btn-primary" >Reset password</button>
					</div>
					<div class="col-sm-4">
						<a  class="btn btn-secondary" href="/tester/login" method="get">Back to Sign In</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>



									