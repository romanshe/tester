﻿<%@ page contentType="text/html; UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<div class="col-lg-2">
	<div class="card">
		<h5 class="card-header text-dark text-center">Hello, ${CURRENT_ACCOUNT}</h5>
		<div class="card-body">
			<ul class="nav flex-column text-center">
				<c:if test="${CURRENT_ROLE_ID == 0}">
					<li class="nav-item">
						<a class="nav-link" href="/tester/admin/account-list">All accounts</a>
					</li>
				</c:if>
				<c:if test="${CURRENT_ROLE_ID == 1 || CURRENT_ROLE_ID == 3}">
					<li class="nav-item">
						<a class="nav-link" href="/tester/student/test-list">All tests</a>
					</li>
				</c:if>
				<c:if test="${CURRENT_ROLE_ID == 1 || CURRENT_ROLE_ID == 2}">
					<li class="nav-item">
						<a class="nav-link" href="/tester/tutor/edit-test-list">Edit tests</a>
					</li>
				</c:if>
				<c:if test="${CURRENT_ROLE_ID == 2}">
					<li class="nav-item">
						<a class="nav-link" href="/tester/tutor/test-list">My tests</a>
					</li>
				</c:if>
				<c:if test="${CURRENT_ROLE_ID == 3}">
					<li class="nav-item">
						<a class="nav-link" href="/tester/student/result-list">My results</a>
					</li>
				</c:if>
				
				<li class="nav-item">
					<a class="nav-link" href="/tester/my-info">My info</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="/tester/logout">Logout</a>
				</li>
			</ul>
		</div>
	</div>
</div>




									