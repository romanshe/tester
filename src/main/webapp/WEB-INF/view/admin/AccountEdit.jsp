﻿<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tag-files" tagdir="/WEB-INF/tags" %>


<div class="col-sm-7">
	<div class="card">
		<h5 class="card-header text-success">Account edit form</h5>
		<div class="card-body">
			<tag-files:AccountForm/>	
			<div class="form-group row">
				<div class="col-sm-6">
					<button type="submit" class="btn btn-primary" >Save changes</button>
				</div>
			</div>
		</div>	
	</div>
</div>

									