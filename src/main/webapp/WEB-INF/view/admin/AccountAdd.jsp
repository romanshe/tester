﻿<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tag-files" tagdir="/WEB-INF/tags" %>


<div class="col-sm-7">
	<div class="card">
		<h5 class="card-header text-success">Add account form</h5>
		<div class="card-body">
			<tag-files:AccountForm/>			
			<div class="form-group row">
				<div class="col-sm-6">
					<button type="submit" class="btn btn-primary" >Add account</button>
				</div>
			</div>
		</div>	
	</div>
</div>