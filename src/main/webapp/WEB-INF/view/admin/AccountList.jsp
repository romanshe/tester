﻿<%@ page contentType="text/html; UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<div class="col-lg-10">
	<div class="card">
		<h5 class="card-header text-dark text-center">Account list</h5>
		<div class="card-body">
			<table class="table">
			  <thead class="thead-dark">
				<tr>
				  <th scope="col">#</th>
				  <th scope="col">First Name</th>
				  <th scope="col">Login</th>
				  <th scope="col">Email</th>
				  <th scope="col">Role</th>
				  <th scope="col">Active</th>
				  <th scope="col">Action</th>

				</tr>
			  </thead>
			  <tbody>
				<c:forEach var="account" items="${ACCOUNT_LIST}">
					<tr>
						<td scope="row">${account.id}</td>
						<td scope="row">${account.firstName}</td>
						<td scope="row">${account.login}</td>
						<td scope="row">${account.email}</td>
						<td scope="row">${account.roles}</td>
						<td scope="row">${account.status}</td>
						  
						<td>
							<a href="/tester/admin/account-edit?idAccount=${account.id }" type="button" class="btn btn-warning">Edit</a>
							<a href="/tester/admin/account-remove?idAccount=${account.id }" type="button" class="btn btn-danger">Remove</a>
							<c:if test="${account.status == true}">	
								<a href="/tester/admin/account-activate?idAccount=${account.id }" type="button" class="btn btn-info">Disasble</a>
							</c:if>
							<c:if test="${account.status == false}">	
								<a href="/tester/admin/account-activate?idAccount=${account.id }" type="button" class="btn btn-info">Enable</a>
							</c:if>
						</td>
					 
					</tr>
				</c:forEach>
			  </tbody>
			</table>
			<div class="row">
				<div class="col-lg-12">
					<div class="row justify-content-center">
						<a href="/tester/admin/add-account" type="button" class="btn btn-primary">Add account</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




									