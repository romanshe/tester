﻿<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="mytags" uri="/WEB-INF/mytags.tld"%>


<div class="col-sm-7">
	<div class="card">
		<h5 class="card-header text-success">Login form
			<a href="/tester/login?LANG=en" method="get">en</a> |
			<a href="/tester/login?LANG=ru" method="get">ru</a>

		</h5>
		<div class="card-body">
			<form action="/tester/login" method="post">		
				<div class="form-group row .bg-light">
					<label for="inputLogin" class="col-sm-3 col-form-label">
						<mytags:message key="login.login" />
					</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="inputLogin" name="LOGIN" placeholder="Login">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword" class="col-sm-3 col-form-label">
						<mytags:message key="login.password" />
					</label>
					<div class="col-sm-9">
						<input type="password" class="form-control" id="inputPassword" name="PASSWORD" placeholder="Password">
						</div>
				</div>
				<div class="form-group row">
					<label for="inputRoled" class="col-sm-3 col-form-label">
						<mytags:message key="login.role"/>
					</label>
					<select class="form-control form-control-sm col-sm-9" name="ROLE">
					  <option value="3" selected>Student</option>
					  <option value="2" >Tutor</option>
					  <option value="1" >Advanced tutor</option>
					  <option value="0" >Admin</option>
					</select>
				</div>
				<div class="form-group row">
					<div class="col-sm-5">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" id="gridCheck1" name="REMEMBER_ME">
								<label class="form-check-label" for="gridCheck1">
									<mytags:message key="login.rememberMe"/>
								</label>
						</div>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-7">
						<a class="stretched-link" href="/tester/forget-password"  method="get"><mytags:message key="login.forgetPassword" /></a>
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-4">
						<button type="submit" class="btn btn-primary" ><mytags:message key="login.signIn" /></button>
					</div>
					<div class="col-sm-4">
						<a class="btn btn-secondary" href="/tester/sign-up" method="get"><mytags:message key="login.signUp" /></a>
					</div>
				</div>
			</form>
		</div>	

	</div>
</div>



									