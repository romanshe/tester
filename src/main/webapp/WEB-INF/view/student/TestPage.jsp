﻿
<div class="col-sm-10">
	<div class="card">
		<h5 class="card-header text-dark text-center">Test page</h5>
		<div class="card-body">
			<h5 class="card-title">${TEST.testName}</h5>
			<p class="card-text">${TEST.description}</p>
			<p class="card-text text-right">Duration per quetion: ${TEST.duration_per_question}</p>

			
			<div class="btn-toolbar justify-content-between" role="toolbar" aria-label="Toolbar with button groups">
			  <div class="btn-group mr-10" role="group" aria-label="Second group">
				<a type="button" class="btn btn-primary" href="/tester/student/online-test-question" >OnLine</a>
				<a type="button" class="btn btn-primary" href="/tester/student/offline-test-question" >OffLine</a>
			  </div>
			<div class="btn-group mr-2" role="group" aria-label="First group">
				<a type="button" class="btn btn-secondary" href="/tester/student/test-list?testReset=true" method="post">To test list</a>
			  </div>
			</div>
		</div>
	</div>
</div>


									