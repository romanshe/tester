﻿
<div class="col-sm-10">
	<div class="card">
		<h5 class="card-header text-dark text-center">Test result</h5>
		<div class="card-body">
			<h5 class="card-title">${TEST.testName}</h5>
			<p class="card-text">${TEST.description}</p>
			<p class="card-text">Test result:  ${TEST_RESULT}</p>
			<p class="card-text text-right">Test duration: ###</p>	
			<div class="btn-toolbar justify-content-between" role="toolbar" aria-label="Toolbar with button groups">
				<div class="btn-group mr-2" role="group" aria-label="First group">
					<a type="button" class="btn btn-secondary" href="/tester/student/test-list">All test</a>
			  </div>
			</div>
		</div>
	</div>
</div>



								