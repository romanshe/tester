﻿<%@ page contentType="text/html; UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<div class="col-sm-10">
	<div class="card">
		<h5 class="card-header text-dark text-center">Test list</h5>
		<div class="card-body">
			<table class="table">
			  <thead class="thead-dark">
				<tr>
				  <th scope="col">#</th>
				  <th scope="col">Name</th>
				  <th scope="col">Description</th>
				  <th scope="col">Duration</th>
				  <th scope="col">Author</th>
				</tr>
			  </thead>
			  <tbody>
				<c:forEach var="test" items="${TEST_LIST}">
					<tr>
					  <th scope="row">${test.id}</th>
					  <td>
						<a href="/tester/student/test-page?IdTest=${test.id}" >${test.testName}</a>
					  </td>
					  <td>${test.description}</td>
					  <td>${test.duration_per_question}</td>
					  <td>${test.authorName}</td>
					</tr>
				</c:forEach>
			  </tbody>
			</table>
		</div>
	</div>
</div>




									