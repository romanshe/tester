﻿<%@ page contentType="text/html; UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<div class="col-sm-10">
	<div class="card">
		<h5 class="card-header text-dark text-center">Result list</h5>
		<div class="card-body">
			<table class="table">
			  <thead class="thead-dark">
				<tr>
				  <th scope="col">#</th>
				  <th scope="col"> Test Name</th>
				  <th scope="col">Result</th>
				  <th scope="col">Data</th>
				</tr>
			  </thead>
			  <tbody>
				<c:forEach var="result" items="${RESULT_LIST}">
					<tr>
					  <th scope="row">${result.id}</th>
					  <td>${result.test_name}</td>
					  <td>${result.percent}</td>
					  <td>${result.created}</td>
					</tr>
				</c:forEach>
			  </tbody>
			</table>
		</div>
	</div>
</div>




									