﻿<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<div class="col-sm-10">
	<div class="card">
		<h5 class="card-header text-dark text-center">${TEST.testName}</h5>
		<div class="card-body">
			<h5 class="card-title">${TEST.description}</h5>
			<table class="table">
			  <tbody>
				<c:forEach var="question" items="${QUESTION_LIST}" varStatus="loop">
					<tr>
						<td>
							<table class="table">
								<thead >
									<tr class="table-active">
										<th scope="col">${loop.index+1}.${question.question}</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<ul class="list-group col-sm-5">
												<c:forEach var="answer" items="${question.answers}">
													<li class="list-group-item">
														<input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
														 <label class="form-check-label" for="defaultCheck1">
															${answer.name}
														</label>
													</li>
												</c:forEach>
											</ul>
										</td>
									</tr>
								</tbody>
							</table>

						</td>
					</tr>
				</c:forEach>
			  </tbody>
			</table>
			<div class="btn-toolbar justify-content-between" role="toolbar" aria-label="Toolbar with button groups">
			  <div class="btn-group mr-10" role="group" aria-label="Second group">
				<button type="button" class="btn btn-primary">Print test</button>
			  </div>
			<div class="btn-group mr-2" role="group" aria-label="First group">
				<a type="button" class="btn btn-secondary" href="/tester/student/test-list?testReset=true" >To test list</a>
			  </div>
			</div>
		</div>
	</div>
</div>




									