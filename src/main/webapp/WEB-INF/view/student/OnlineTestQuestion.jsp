﻿<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<div class="col-sm-10">
	<div class="card">
		<h5 class="card-header text-dark text-center">${TEST.testName}</h5>
		<div class="card-body">
			<form action="/tester/student/online-test-question" method="get">	
				<h5 class="card-title">Question ${CURRENT_QUESTION_NUM+1} of ${QUESTION_LIST.size()}</h5>
				<p class="card-text">${QUESTION_LIST.get(CURRENT_QUESTION_NUM).question}</p>
				
				<ul class="list-group col-sm-12">
					<c:forEach var="answer" items="${QUESTION_LIST.get(CURRENT_QUESTION_NUM).answers}">
						<li class="list-group-item">
							<input class="form-check-input" type="checkbox" name="ANSWER_CHECKBOX_${answer.id}" id="defaultCheck1">
							 <label class="form-check-label" for="defaultCheck1">
								${answer.name}
							</label>
						</li>
					</c:forEach>
					
				</ul>

				<p class="card-text text-right">Test duration: ###</p>

				
				<div class="btn-toolbar justify-content-between" role="toolbar" aria-label="Toolbar with button groups">
				  <div class="btn-group mr-10" role="group" aria-label="Second group">	
					<c:if test="${CURRENT_QUESTION_NUM > 0}">			  
						<button type="submit" class="btn btn-primary" name="direction" value="false">Previous Question</button>
					</c:if>
					<c:if test="${CURRENT_QUESTION_NUM < QUESTION_LIST.size()-1}">	
						<button type="submit" class="btn btn-primary" name="direction" value="true">Next Question</button>
					</c:if>
				  </div>
				<div class="btn-group mr-2" role="group" aria-label="First group">
					<button type="submit" class="btn btn-secondary" name="FINISH_BUTTON" value="true">Finish test</button>
				  </div>
				</div>
			</form>
		</div>
	</div>
</div>



									