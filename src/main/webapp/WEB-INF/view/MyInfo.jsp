﻿<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<div class="col-sm-7">
	<div class="card">
		<h5 class="card-header text-success">My info form</h5>
		<div class="card-body">
			<form action="/tester/login" method="post">		
				<div class="form-group row .bg-light">
					<label for="inputLogin" class="col-sm-3 col-form-label">
						Login
					</label>
					<div class="col-sm-9">
						<input type="text" class="form-control" id="inputLogin" name="LOGIN" placeholder=${CURRENT_ACCOUNT.login}>
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword" class="col-sm-3 col-form-label">
						Password
					</label>
					<div class="col-sm-9">
						<input type="password" class="form-control" id="inputPassword" name="Password" placeholder="Password">
						</div>
				</div>
				<div class="form-group row">
					<label for="inputFirstName" class="col-sm-3 col-form-label">
						First Name
					</label>
					<div class="col-sm-9">
						<input class="form-control" id="inputFirstName" name="FirstName" placeholder=${CURRENT_ACCOUNT.firstName}>
						</div>
				</div>
				<div class="form-group row">
					<label for="inputLastName" class="col-sm-3 col-form-label">
						Last Name
					</label>
					<div class="col-sm-9">
						<input class="form-control" id="inputLastName" name="LastName" placeholder=${CURRENT_ACCOUNT.lastName}>
						</div>
				</div>
				<div class="form-group row">
					<label for="inputMiddleName" class="col-sm-3 col-form-label">
						Middle Name
					</label>
					<div class="col-sm-9">
						<input class="form-control" id="inputMiddleName" name="MiddleName" placeholder=${CURRENT_ACCOUNT.middleName}>
						</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail" class="col-sm-3 col-form-label">
						Email
					</label>
					<div class="col-sm-9">
						<input class="form-control" id="inputEmail" name="Email" placeholder=${CURRENT_ACCOUNT.email}>
						</div>
				</div>
				<div class="form-group row">
					<label for="inputRoled" class="col-sm-3 col-form-label">
						Role
					</label>
					<select class="form-control form-control-sm col-sm-9" name="Role">
					  <option>STUDENT</option>
					  <option>TUTOR</option>
					  <option>ADVANCEDTUTOR</option>
					  <option>ADMIN</option>
					</select>
				</div>

				<div class="form-group row">
					<div class="col-sm-6">
						<button type="submit" class="btn btn-primary" >Save changes????</button>
					</div>
				</div>
			</form>
		</div>	

	</div>
</div>

									