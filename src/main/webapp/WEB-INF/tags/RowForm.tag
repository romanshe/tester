<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ attribute name="type" required="true" type="java.lang.String" %>
<%@ attribute name="label" required="true" type="java.lang.String" %>
<%@ attribute name="attribute_name" required="true" type="java.lang.String" %>
<%@ attribute name="value" required="false" type="java.lang.String" %>

<div class="form-group row">
    <label for="input" class="col-sm-3 col-form-label">
        ${label}
    </label>
    <div class="col-sm-9">
        <input type="${type}" class="form-control" id="input" name="${attribute_name}" ${value}>
    </div>
</div>