<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<div class="form-group row">
        <label for="selectRole" class="col-sm-3 col-form-label">
            Role
        </label>
        <ul class="list-group list-group-horizontal col-9">
            <li class="list-group-item col-2">
                <input class="form-check-input" type="checkbox" name="STUDENT_ROLE" <c:if test="${STUDENT_ROLE}">checked</c:if> >
                 <label class="form-check-label" >
                    Student
                </label>
            </li>
            <li class="list-group-item col-2">
                <input class="form-check-input" type="checkbox" name="TUTOR_ROLE" <c:if test="${TUTOR_ROLE}">checked</c:if>>
                 <label class="form-check-label" >
                    Tutor
                </label>
            </li>
            <li class="list-group-item col-2">
                <input class="form-check-input" type="checkbox" name="ADVANCED_TUTOR_ROLE" <c:if test="${ADVANCED_TUTOR_ROLE}">checked</c:if>>
                 <label class="form-check-label" >
                    Advanced tutor
                </label>
            </li>
            <li class="list-group-item col-2">
                <input class="form-check-input" type="checkbox" name="ADMIN_ROLE" <c:if test="${ADMIN_ROLE}">checked</c:if>>
                 <label class="form-check-label" >
                    Admin
                </label>
            </li>
        </ul>
</div>