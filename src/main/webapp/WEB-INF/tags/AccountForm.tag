<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tag-files" tagdir="/WEB-INF/tags" %>

<%@ attribute name="message" required="false" type="java.lang.String" %>
<%@ attribute name="title" required="false" type="java.lang.String" %>

<c:choose>
    <c:when test="${EDIT_ACCOUNT!= null}">
        <c:set var="loginValue" value="value=${EDIT_ACCOUNT.login}"/>
        <c:set var="passwordValue" value="placeholder='Password'"/>
        <c:set var="firstNameValue" value="value=${EDIT_ACCOUNT.firstName}"/>
        <c:set var="lastNameValue" value="value=${EDIT_ACCOUNT.lastName}"/>
        <c:set var="middleNameValue" value="value=${EDIT_ACCOUNT.middleName}"/>
        <c:set var="emailValue" value="value=${EDIT_ACCOUNT.email}"/>
        <c:set var="formActionParameter" value="?idAccount=${EDIT_ACCOUNT.id }"/>
    </c:when>
    <c:otherwise>
        <c:set var="loginValue" value="placeholder='Login'"/>
        <c:set var="passwordValue" value="placeholder='Password'"/>
        <c:set var="firstNameValue" value="placeholder='First Name'"/>
        <c:set var="lastNameValue" value="placeholder='Last Name'"/>
        <c:set var="middleNameValue" value="placeholder='Middle Name'"/>
        <c:set var="emailValue" value="placeholder='Email'"/>
        <c:set var="formActionParameter" value=""/>
    </c:otherwise>
</c:choose>

<form action="/tester/admin/account-edit${formActionParameter}" method="post">
    <tag-files:RowForm>
        <jsp:attribute name="type">text</jsp:attribute>
        <jsp:attribute name="label">Login</jsp:attribute>
        <jsp:attribute name="attribute_name">LOGIN</jsp:attribute>
        <jsp:attribute name="value">${loginValue}</jsp:attribute>
    </tag-files:RowForm>
    <tag-files:RowForm>
        <jsp:attribute name="type">password</jsp:attribute>
        <jsp:attribute name="label">Password</jsp:attribute>
        <jsp:attribute name="attribute_name">PASSWORD</jsp:attribute>
        <jsp:attribute name="value">${passwordValue}</jsp:attribute>
    </tag-files:RowForm>
    <tag-files:RowForm>
        <jsp:attribute name="type">text</jsp:attribute>
        <jsp:attribute name="label">First Name</jsp:attribute>
        <jsp:attribute name="attribute_name">FIRST_NAME</jsp:attribute>
        <jsp:attribute name="value">${firstNameValue}</jsp:attribute>
    </tag-files:RowForm>
    <tag-files:RowForm>
        <jsp:attribute name="type">text</jsp:attribute>
        <jsp:attribute name="label">Last Name</jsp:attribute>
        <jsp:attribute name="attribute_name">LAST_NAME</jsp:attribute>
        <jsp:attribute name="value">${lastNameValue}</jsp:attribute>
    </tag-files:RowForm>
    <tag-files:RowForm>
        <jsp:attribute name="type">text</jsp:attribute>
        <jsp:attribute name="label">Middle Name</jsp:attribute>
        <jsp:attribute name="attribute_name">MIDDLE_NAME</jsp:attribute>
        <jsp:attribute name="value">${middleNameValue}</jsp:attribute>
    </tag-files:RowForm>
    <tag-files:RowForm>
        <jsp:attribute name="type">text</jsp:attribute>
        <jsp:attribute name="label">Email</jsp:attribute>
        <jsp:attribute name="attribute_name">EMAIL</jsp:attribute>
        <jsp:attribute name="value">${emailValue}</jsp:attribute>
    </tag-files:RowForm>
    <tag-files:RoleSelectForm/>
</form>
