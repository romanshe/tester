package it.source.tester.entity;

import lombok.Getter;
import lombok.Setter;

public class Answer {
    @Getter @Setter private Long id;
    @Getter @Setter private Long id_question;
    @Getter @Setter private String name;
    @Getter @Setter private boolean correct;

}
