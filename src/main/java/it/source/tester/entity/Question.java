package it.source.tester.entity;

import lombok.*;

import java.util.List;

public class Question {
    @Getter @Setter private Long id;
    @Getter @Setter private Long id_test;
    @Getter @Setter private String question;
    @Getter @Setter private List<Answer> answers;
}
