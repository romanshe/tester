package it.source.tester.entity;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

public class Account {
    @Getter @Setter private Long id;
    @Getter @Setter private String login;
    @Getter @Setter private String password;
    @Getter @Setter private List<Role> roles;
    @Getter @Setter private String firstName;
    @Getter @Setter private String lastName;
    @Getter @Setter private String middleName;
    @Getter @Setter private String email;
    @Getter @Setter private boolean status;

    public Account(String login, String password, List<Role> roles) {
        this.login = login;
        this.password = password;
        this.roles = roles;
    }

    public Account() {
    }

    public List<Long> getRoleIdList(){
        List<Long> roleId = new ArrayList();
        for (Role role : roles) {
            roleId.add(role.getId());
        }
        return roleId;
    }

    @Override
    public String toString() {
        return getLogin();
    }




}
