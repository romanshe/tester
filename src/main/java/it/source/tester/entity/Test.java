package it.source.tester.entity;

import lombok.*;

public class Test {
    @Getter @Setter private Long id;
    @Getter @Setter private String testName;
    @Getter @Setter private short duration_per_question;
    @Getter @Setter private Long id_account;
    @Getter @Setter private String description;
    @Getter @Setter private String authorName;
}
