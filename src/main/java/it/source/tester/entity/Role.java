package it.source.tester.entity;

import lombok.Data;

import java.util.Objects;

@Data
public class Role {
    String name;
    Long id;

    public Role(Long id){
        this.id = id;
    }

    public Role(Long id, String name){
        this.setId(id);
        this.setName(name);
    }

    public  Role(){
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return id == role.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return name;
    }
}
