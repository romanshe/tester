package it.source.tester.entity;

import lombok.Data;

import java.sql.Date;

@Data
public class Result {
    long id_test;
    double percent;
    long id_account;
    long id;
    String test_name;
    Date created;
}
