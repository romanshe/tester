package it.source.tester.entity;

public enum RoleEnum {
    STUDENT, TUTOR, ADVANCEDTUTOR, ADMIN;

    public boolean isValidRole(RoleEnum role){
        if (this == role) return true;
        if (this == TUTOR && role == STUDENT) return true;
        if (this == ADVANCEDTUTOR && (role == TUTOR || role == STUDENT)) return true;
        if (this == ADMIN) return true;
        return false;
    }

    public static RoleEnum parseString(String string){
        if (string.equals("STUDENT")) return STUDENT;
        if (string.equals("TUTOR")) return TUTOR;
        if (string.equals("ADVANCEDTUTOR")) return ADVANCEDTUTOR;
        if (string.equals("ADMIN")) return ADMIN;
        return null;
    }
}
