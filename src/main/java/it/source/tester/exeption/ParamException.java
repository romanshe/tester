package it.source.tester.exeption;

public class ParamException extends Exception{
    public ParamException(String message) {
        super(message);
    }

    public ParamException(String message, Throwable cause) {
        super(message, cause);
    }
}
