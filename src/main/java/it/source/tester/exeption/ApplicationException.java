package it.source.tester.exeption;

public class ApplicationException extends RuntimeException {
    public ApplicationException(String message) {
    }

    public ApplicationException(Throwable cause) {
    }

    public ApplicationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    }

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }
}
