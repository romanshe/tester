package it.source.tester.exeption;

public class CookieException extends Exception  {
    public CookieException(String message) {
        super(message);
    }
}
