package it.source.tester.exeption;

public class ExistRecordException extends Exception{
    public ExistRecordException(String message) {
        super(message);
    }
}
