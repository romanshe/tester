package it.source.tester.exeption;

public class ValidationException extends Exception  {
    public ValidationException(String message) {
        super(message);
    }
}
