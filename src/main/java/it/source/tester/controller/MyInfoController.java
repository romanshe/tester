package it.source.tester.controller;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

@WebServlet("/my-info")
public class MyInfoController extends AbstractController {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet my-info");

        forwardToPage(PAGE_MY_INFO, req, resp);
    }
}
