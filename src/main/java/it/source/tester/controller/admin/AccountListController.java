package it.source.tester.controller.admin;

import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Account;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static it.source.tester.Constants.*;


@WebServlet("/admin/account-list")
public class AccountListController extends AbstractController {
    private static final Logger LOG = LoggerFactory.getLogger(AccountListController.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("doGet account-list");

        List<Account> accountList = getServiceManager().getAdminService().findAll();

        req.setAttribute(ATTRIBUTE_ACCOUNT_LIST, accountList);

        forwardToPage(PAGE_ACCOUNT_LIST, req, resp);
    }
}
