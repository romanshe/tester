package it.source.tester.controller.admin;

import it.source.tester.Util.Util;
import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Account;
import it.source.tester.entity.Role;
import it.source.tester.exeption.ParamException;
import it.source.tester.exeption.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import static it.source.tester.Constants.*;


@WebServlet("/admin/account-edit")
public class AccountEditController extends AbstractController {
    private static final Logger LOG = LoggerFactory.getLogger(AccountEditController.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("doGet account-edit");

        long idAccount = Long.valueOf(req.getParameter(PARAMETER_ID_ACCOUNT));
        try {
            Account account = getServiceManager().getAdminService().findById(idAccount);
            req.setAttribute(ATTRIBUTE_EDIT_ACCOUNT, account);
            List<Role> roles = account.getRoles();
            if (roles != null) {
                req.setAttribute(ATTRIBUTE_STUDENT_ROLE, roles.contains(STUDENT_ROLE));
                req.setAttribute(ATTRIBUTE_TUTOR_ROLE, roles.contains(TUTOR_ROLE));
                req.setAttribute(ATTRIBUTE_ADVANCED_TUTOR_ROLE, roles.contains(ADVANCED_TUTOR_ROLE));
                req.setAttribute(ATTRIBUTE_ADMIN_ROLE, roles.contains(ADMIN_ROLE));
            }

        } catch (ValidationException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
            LOG.warn("doGet account-edit error: {}", e.getMessage());
        }
        forwardToPage(PAGE_EDIT_ACCOUNT, req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("doPost account-edit");

        long idAccount = Long.valueOf(req.getParameter(PARAMETER_ID_ACCOUNT));
        String login = req.getParameter(PARAMETER_LOGIN);
        String password = req.getParameter(PARAMETER_PASSWORD);
        String firstName = req.getParameter(PARAMETER_FIRST_NAME);
        String lastName = req.getParameter(PARAMETER_LAST_NAME);
        String middleName = req.getParameter(PARAMETER_MIDDLE_NAME);
        String email = req.getParameter(PARAMETER_EMAIL);
        String roleAdmin = req.getParameter(PARAMETER_ADMIN_ROLE);
        String roleAdvancedTutor = req.getParameter(PARAMETER_ADVANCED_TUTOR_ROLE);
        String roleTutor = req.getParameter(PARAMETER_TUTOR_ROLE);
        String roleStudent = req.getParameter(PARAMETER_STUDENT_ROLE);

        try {
            Account account = getServiceManager().getAdminService().findById(idAccount);
            List<Role> roles = Util.setRoles(roleAdmin, roleAdvancedTutor, roleTutor, roleStudent);
            Util.setAccount(account, login,password, firstName, lastName, middleName, email);
            getServiceManager().getAdminService().updateAccount(account);

            if (account.getRoles() != null) {
                List<Role> rolesSub = new ArrayList(account.getRoles());
                rolesSub.removeAll(roles);
                if (rolesSub.size() > 0) {
                    getServiceManager().getAdminService().deleteRoles(account.getId(), rolesSub);
                }
            }

            List<Role> rolesSub = new ArrayList(roles);
            if (account.getRoles() != null) {
                rolesSub.removeAll(account.getRoles());
            }
            if (rolesSub.size() > 0) {
                getServiceManager().getAdminService().addRoles(account.getId(), rolesSub);
            }

            req.setAttribute(ATTRIBUTE_EDIT_ACCOUNT, account);
            sendRedirect("/admin/account-list", resp);
        } catch (ParamException | ValidationException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
            LOG.warn("doPost account-edit error: {}", e.getMessage());
            forwardToPage(PAGE_EDIT_ACCOUNT, req, resp);
        }





    }
}
