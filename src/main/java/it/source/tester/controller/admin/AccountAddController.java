package it.source.tester.controller.admin;

import it.source.tester.Util.Util;
import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Account;
import it.source.tester.entity.Role;
import it.source.tester.exeption.ExistRecordException;
import it.source.tester.exeption.ParamException;
import it.source.tester.exeption.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import static it.source.tester.Constants.*;

@WebServlet("/admin/add-account")
public class AccountAddController extends AbstractController {
    private static final Logger LOG = LoggerFactory.getLogger(AccountAddController.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("doGet /admin/add-account");
        forwardToPage(PAGE_ADD_ACCOUNT, req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("doPost /admin/add-account");

        String login = req.getParameter(PARAMETER_LOGIN);
        String password = req.getParameter(PARAMETER_PASSWORD);
        String firstName = req.getParameter(PARAMETER_FIRST_NAME);
        String lastName = req.getParameter(PARAMETER_LAST_NAME);
        String middleName = req.getParameter(PARAMETER_MIDDLE_NAME);
        String email = req.getParameter(PARAMETER_EMAIL);
        String roleAdmin = req.getParameter(PARAMETER_ADMIN_ROLE);
        String roleAdvancedTutor = req.getParameter(PARAMETER_ADVANCED_TUTOR_ROLE);
        String roleTutor = req.getParameter(PARAMETER_TUTOR_ROLE);
        String roleStudent = req.getParameter(PARAMETER_STUDENT_ROLE);

        try {
            Account account = new Account();
            List<Role> roles = Util.setRoles(roleAdmin, roleAdvancedTutor, roleTutor, roleStudent);
            Util.setAccount(account, login,password, firstName, lastName, middleName, email);
            getServiceManager().getAdminService().addAccount(account);
            account = getServiceManager().getAdminService().findByLogin(account.getLogin());
            getServiceManager().getAdminService().addRoles(account.getId(), roles);
            sendRedirect("/admin/account-list", resp);
        } catch (ParamException | ValidationException | ExistRecordException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
            LOG.warn("doPost /admin/add-account error:{}", e.getMessage());
            forwardToPage(PAGE_ADD_ACCOUNT, req, resp);
        }

    }
}
