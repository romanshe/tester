package it.source.tester.controller.admin;

import it.source.tester.controller.AbstractController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

@WebServlet("/admin/account-remove")
public class AccountRemoveController extends AbstractController {
    private static final Logger LOG = LoggerFactory.getLogger(AccountRemoveController.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("doGet account-remove");

        long idAccount = Long.valueOf(req.getParameter(PARAMETER_ID_ACCOUNT));
        int count = getServiceManager().getAdminService().removeById(idAccount);
        sendRedirect("/admin/account-list", resp);
    }
}
