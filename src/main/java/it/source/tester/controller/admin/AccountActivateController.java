package it.source.tester.controller.admin;

import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Account;
import it.source.tester.exeption.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.PARAMETER_ID_ACCOUNT;

@WebServlet("/admin/account-activate")
public class AccountActivateController extends AbstractController {
    private static final Logger LOG = LoggerFactory.getLogger(AccountActivateController.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("doGet account-activate");

        long idAccount = Long.valueOf(req.getParameter(PARAMETER_ID_ACCOUNT));
        try {
            Account account = getServiceManager().getAdminService().findById(idAccount);
            getServiceManager().getAdminService().setStatusById(idAccount,!account.isStatus());
        } catch (ValidationException e) {
        }
        sendRedirect("/admin/account-list", resp);
    }
}
