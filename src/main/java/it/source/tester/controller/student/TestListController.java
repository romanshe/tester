package it.source.tester.controller.student;

import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Account;
import it.source.tester.entity.Test;
import it.source.tester.exeption.ValidationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static it.source.tester.Constants.*;

@WebServlet("/student/test-list")
public class TestListController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet test-list");
        boolean testReset = Boolean.parseBoolean(req.getParameter(PARAMETER_TEST_RESET));
        if (testReset){
            req.setAttribute(ATTRIBUTE_TEST, null);
            req.setAttribute(ATTRIBUTE_QUESTION_LIST, null);
        }

        Account account = (Account) req.getSession().getAttribute(ATTRIBUTE_CURRENT_ACCOUNT);
        Long roleId = (Long) req.getSession().getAttribute(ATTRIBUTE_CURRENT_ROLE_ID);
        try {
            List<Test> testList;
            if (roleId.equals(TUTOR_ROLE_ID)){
                testList = getServiceManager().getUserService().getTestList(account.getId());
            } else {
                testList = getServiceManager().getUserService().getTestList();
            }
            req.setAttribute(ATTRIBUTE_TEST_LIST, testList);
        } catch (ValidationException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
        }
        forwardToPage(PAGE_TEST_LIST, req, resp);

    }
}
