package it.source.tester.controller.student;

import it.source.tester.controller.AbstractController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

@WebServlet("/student/offline-test-question")
public class OfflineTestQuestionController extends AbstractController {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet offline-test-question");

        forwardToPage(PAGE_OFF_LINE_TEST_QUESTION, req, resp);

    }
}
