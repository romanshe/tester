package it.source.tester.controller.student;

import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Account;
import it.source.tester.entity.Result;
import it.source.tester.exeption.ValidationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static it.source.tester.Constants.*;

@WebServlet("/student/result-list")
public class ResultListController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet result-list");
        Account account = (Account)req.getSession().getAttribute(ATTRIBUTE_CURRENT_ACCOUNT);

        try {
            List<Result> resultList = getServiceManager().getUserService().getResultList(account.getId());
            req.setAttribute(ATTRIBUTE_RESULT_LIST, resultList);
        } catch (ValidationException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
        }

        forwardToPage(PAGE_RESULT_LIST, req, resp);

    }
}
