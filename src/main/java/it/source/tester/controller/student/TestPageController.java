package it.source.tester.controller.student;
import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Question;
import it.source.tester.entity.Test;
import it.source.tester.exeption.ValidationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import static it.source.tester.Constants.*;

@WebServlet("/student/test-page")
public class TestPageController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet test-page");

        long idTest = Long.valueOf(req.getParameter(PARAMETER_ID_TEST));

        try {
            Test test = getServiceManager().getUserService().getTest(idTest);
            req.getSession().setAttribute(ATTRIBUTE_TEST, test);

            List<Question> questionList = getServiceManager().getUserService().getQuestionList(idTest);
            ArrayList<Integer> studentAnswers = new ArrayList<Integer>(Collections.nCopies(questionList.size(), NOT_CORRECT_ANSWER_VALUE));
            req.getSession().setAttribute(ATTRIBUTE_QUESTION_LIST, questionList);
            req.getSession().setAttribute(ATTRIBUTE_CORRECT_ANSWER_LIST, studentAnswers);
            req.getSession().setAttribute(ATTRIBUTE_CURRENT_QUESTION_NUM, FIRST_QUESTION_NUMBER);

        } catch (ValidationException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
        }


        forwardToPage(PAGE_TEST_PAGE, req, resp);
    }
}
