package it.source.tester.controller.student;

import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Answer;
import it.source.tester.entity.Question;
import it.source.tester.entity.Test;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static it.source.tester.Constants.*;

@WebServlet("/student/online-test-question")
public class OnlineTestQuestionController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet online-test-question");

        int currentQuestionNum = getCurrentQuestionNum(req);
        List<Question> questionList = (List<Question>)req.getSession().getAttribute(ATTRIBUTE_QUESTION_LIST);
        checkAnswer(req, currentQuestionNum, questionList.get(currentQuestionNum).getAnswers());

        Boolean isFinishTestButton = Boolean.parseBoolean(req.getParameter(PARAMETER_FINISH_BUTTON));
        if (isFinishTestButton){
            sendRedirect("/student/finish-test", resp);
        } else {
            int nextQuestionNum = getNextQuestionNum(req, currentQuestionNum, questionList.size());
            req.getSession().setAttribute(ATTRIBUTE_CURRENT_QUESTION_NUM, nextQuestionNum);
            forwardToPage(PAGE_ON_LINE_TEST_QUESTION, req, resp);
        }
    }



    private static int getCurrentQuestionNum(HttpServletRequest req){
        Integer currentQuestionNum = (Integer) req.getSession().getAttribute(ATTRIBUTE_CURRENT_QUESTION_NUM);
        if (currentQuestionNum==null){
            currentQuestionNum=FIRST_QUESTION_NUMBER;
        }
        return currentQuestionNum;
    }

    private static int getNextQuestionNum(HttpServletRequest req, Integer currentQuestionNum, int size){
        String paramTestDirection = req.getParameter(PARAMETER_TEST_DIRECTION);
        if (StringUtils.isNotEmpty(paramTestDirection)){
            boolean direction = Boolean.parseBoolean(paramTestDirection);
            if (direction) {
                if (currentQuestionNum < size - 1)
                    currentQuestionNum++;
            } else {
                if (currentQuestionNum > 1)
                    currentQuestionNum--;
            }
        }
        return currentQuestionNum;
    }

    private static void checkAnswer(HttpServletRequest req, int currentQuestionNum, List<Answer> answerList){
         List<Integer> studentAnswers = (List<Integer>)req.getSession().getAttribute(ATTRIBUTE_CORRECT_ANSWER_LIST);

        int result = NOT_CORRECT_ANSWER_VALUE;
        for (Answer answer : answerList) {
            String answerCheckbox = req.getParameter(PARAMETER_ANSWER_CHECKBOX_ID + answer.getId());
            if (StringUtils.isNotEmpty(answerCheckbox)) {
                Boolean studentAnswer = answerCheckbox.equals("on");
                if (answer.isCorrect() && studentAnswer) result = CORRECT_ANSWER_VALUE;
                if (answer.isCorrect() && !studentAnswer) result = NOT_CORRECT_ANSWER_VALUE;
                if (!answer.isCorrect() && studentAnswer) result = NOT_CORRECT_ANSWER_VALUE;
            }
        }
        studentAnswers.set(currentQuestionNum, result);
    }
}
