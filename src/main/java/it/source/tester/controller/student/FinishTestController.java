package it.source.tester.controller.student;

import it.source.tester.Util.Util;
import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Account;
import it.source.tester.entity.Result;
import it.source.tester.entity.Test;
import it.source.tester.exeption.ValidationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static it.source.tester.Constants.*;

@WebServlet("/student/finish-test")
public class FinishTestController extends AbstractController {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet finish-test");

        List<Integer> studentAnswers = (List<Integer>)req.getSession().getAttribute(ATTRIBUTE_CORRECT_ANSWER_LIST);
        double studentResult = ((studentAnswers.stream().mapToDouble(i -> i).sum()) / studentAnswers.size()) * 100.0;

        Test test = (Test) req.getSession().getAttribute(ATTRIBUTE_TEST);
        Account account = (Account)req.getSession().getAttribute(ATTRIBUTE_CURRENT_ACCOUNT);
        Result result1 = Util.setResult(test,account, studentResult);

        try {
            getServiceManager().getUserService().addResult(result1);
        } catch (ValidationException e) {
            e.printStackTrace();
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
        }
        req.setAttribute(ATTRIBUTE_TEST_RESULT, studentResult);
        forwardToPage(PAGE_TEST_RESULT, req, resp);
    }
}
