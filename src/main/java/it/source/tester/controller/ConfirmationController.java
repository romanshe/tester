package it.source.tester.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

@WebServlet("/confirmation")
public class ConfirmationController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String token = req.getParameter(PARAMETER_UUID);
        getServiceManager().getAdminService().accountConfirmation(token);
        forwardToPage(PAGE_LOGIN, req, resp);
    }
}
