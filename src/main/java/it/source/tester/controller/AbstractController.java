package it.source.tester.controller;

import it.source.tester.Services.impl.ServiceManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

public class AbstractController extends HttpServlet {

    private ServiceManager serviceManager;

    @Override
    public void init() throws ServletException {
        serviceManager = ServiceManager.getInstance(getServletContext());
    }

    protected ServiceManager getServiceManager(){
        return serviceManager;
    }

    protected void forwardToPage(String pageName, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("currentPage", pageName);
        req.getRequestDispatcher(PAGE_TEMPLATE).forward(req, resp);
    }

    protected void sendRedirect(String req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect(APP_CONTEXT_PATH + req);
    }
}
