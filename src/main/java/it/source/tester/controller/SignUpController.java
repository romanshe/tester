package it.source.tester.controller;

import it.source.tester.Util.Util;
import it.source.tester.entity.Account;
import it.source.tester.exeption.ExistRecordException;
import it.source.tester.exeption.ParamException;
import it.source.tester.exeption.ValidationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;

import static it.source.tester.Constants.*;

@WebServlet("/sign-up")
public class SignUpController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet /sign-up");
        forwardToPage(PAGE_SIGN_UP, req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doPost /sign-up");

        String login = req.getParameter(PARAMETER_LOGIN);
        String password = req.getParameter(PARAMETER_PASSWORD);
        String firstName = req.getParameter(PARAMETER_FIRST_NAME);
        String lastName = req.getParameter(PARAMETER_LAST_NAME);
        String middleName = req.getParameter(PARAMETER_MIDDLE_NAME);
        String email = req.getParameter(PARAMETER_EMAIL);

        try {
            Account account = new Account();
            Util.setAccount(account, login,password, firstName, lastName, middleName, email);
            getServiceManager().getAdminService().addAccount(account);
            account = getServiceManager().getAdminService().findByLogin(account.getLogin());
            getServiceManager().getAdminService().addRoles(account.getId(), Arrays.asList(STUDENT_ROLE));
            UUID uuid = UUID.randomUUID();
            getServiceManager().getAdminService().addToken(account.getId(), uuid.toString());
            getServiceManager().getNotificationService().sendNotificationMessage(email, "content http://localhost:8080/tester/confirmation?UUID=" + uuid);
            sendRedirect("/login", resp);
        } catch (ParamException | ExistRecordException | ValidationException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
            forwardToPage(PAGE_SIGN_UP, req, resp);
        }

    }
}

