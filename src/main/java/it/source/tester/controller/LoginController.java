package it.source.tester.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import it.source.tester.Util.CookieUtil;
import it.source.tester.entity.Account;
import it.source.tester.entity.Role;
import it.source.tester.exeption.CookieException;
import it.source.tester.exeption.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static it.source.tester.Constants.*;

@WebServlet("/login")
public class LoginController extends AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("doGet login");

        Long currentRoleId;// = (Long) req.getSession().getAttribute(ATTRIBUTE_CURRENT_ROLE_ID);
//        if (currentRoleId != null) {
//            sendRedirect(roleHomePage(currentRoleId), resp);
//        } else {
            try {
                Long id = Long.valueOf(CookieUtil.getCookieValue(COOKIE_REMEMBER_ME_ID, req));
                currentRoleId = Long.valueOf(CookieUtil.getCookieValue(COOKIE_REMEMBER_ME_ROLE_ID, req));
                Account account = getServiceManager().getAdminService().findById(id);
                req.getSession().setAttribute(ATTRIBUTE_CURRENT_ACCOUNT, account);
                req.getSession().setAttribute(ATTRIBUTE_CURRENT_ROLE_ID, currentRoleId);
                sendRedirect(roleHomePage(currentRoleId), resp);
            } catch (CookieException ex) {
                forwardToPage(PAGE_LOGIN, req, resp);
            } catch (ValidationException e) {
                req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
                forwardToPage(PAGE_LOGIN, req, resp);
            }
        //}
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOG.info("doPost login");

        String login = req.getParameter(PARAMETER_LOGIN);
        String password = req.getParameter(PARAMETER_PASSWORD);
        Long currentRoleId = Long.parseLong(req.getParameter(PARAMETER_ROLE));
        Role currentRole = new Role(currentRoleId);
        String rememberMeString = req.getParameter(PARAMETER_REMEMBER_ME);
        Boolean rememberMe = false;
        if (rememberMeString != null) {
            if (rememberMeString.equals("on")) {
                rememberMe = true;
            }
        }


        try {
            Account account = getServiceManager().getCommonService().login(login, password);

            req.getSession().setAttribute(ATTRIBUTE_CURRENT_ACCOUNT, account);
            req.getSession().setAttribute(ATTRIBUTE_CURRENT_ROLE_ID, currentRoleId);
            if (rememberMe) {
                CookieUtil.addCookie(COOKIE_REMEMBER_ME_ID, account.getId().toString(), resp);
                CookieUtil.addCookie(COOKIE_REMEMBER_ME_ROLE_ID, currentRoleId.toString(), resp);
            }
            if (account.getRoles().contains(currentRole) && account.isStatus()) {
                sendRedirect(roleHomePage(currentRoleId), resp);
            } else {
                req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, "Not a valid role for this account or account was deactivated");
                LOG.warn("Not a valid role for this account or account was deactivated");
                forwardToPage(PAGE_LOGIN, req, resp);
            }

        } catch (ValidationException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
            forwardToPage(PAGE_LOGIN, req, resp);
        }
    }

    private static String roleHomePage(Long currentRoleId) {
        String result = "";
        if (currentRoleId.equals(STUDENT_ROLE_ID)) {
            result = "/student/test-list";
        } else if (currentRoleId.equals(TUTOR_ROLE_ID)) {
            result = "/student/test-list";
        } else if (currentRoleId.equals(ADVANCED_TUTOR_ROLE_ID)) {
            result = "/tutor/edit-test-list";
        } else if (currentRoleId.equals(ADMIN_ROLE_ID)) {
            result = "/admin/account-list";
        }
        return result;
    }
}
