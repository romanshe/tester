package it.source.tester.controller.tutor;

import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Question;
import it.source.tester.exeption.ValidationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static it.source.tester.Constants.*;

@WebServlet("/tutor/edit-question-list")
public class EditQuestionListController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet edit-question-list");

        long idTest = Long.valueOf(req.getParameter(PARAMETER_ID_TEST));

        try {
            List<Question> questionList = getServiceManager().getUserService().getQuestionList(idTest);
            req.setAttribute(ATTRIBUTE_QUESTION_LIST, questionList);
        } catch (ValidationException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
            req.setAttribute(ATTRIBUTE_QUESTION_LIST, null);
        }
        req.setAttribute(PARAMETER_ID_TEST,idTest);
        forwardToPage(PAGE_EDIT_QUESTION_LIST, req, resp);
    }
}
