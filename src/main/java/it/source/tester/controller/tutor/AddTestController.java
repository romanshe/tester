package it.source.tester.controller.tutor;

import it.source.tester.Util.Util;
import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Test;
import it.source.tester.exeption.ExistRecordException;
import it.source.tester.exeption.ParamException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

@WebServlet("/tutor/add-test")
public class AddTestController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet add-test");
        forwardToPage(PAGE_ADD_TEST, req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doPost add-test");

        String testDescription = req.getParameter(PARAMETER_TEST_DESCRIPTION);
        String testName = req.getParameter(PARAMETER_TEST_NAME);
        String testDuration = req.getParameter(PARAMETER_TEST_DURATION);

        try {
            Test test = new Test();
            Util.setTest(test, testName, testDescription, testDuration);
            getServiceManager().getTutorService().addTest(test);
            sendRedirect("/tutor/edit-test-list", resp);
        } catch (ParamException | ExistRecordException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
            forwardToPage(PAGE_ADD_TEST, req, resp);
        }


    }
}
