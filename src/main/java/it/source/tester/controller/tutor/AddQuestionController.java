package it.source.tester.controller.tutor;

import it.source.tester.Util.Util;
import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Question;
import it.source.tester.exeption.ParamException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

@WebServlet("/tutor/add-question")
public class AddQuestionController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet add-question");

        String idTest = req.getParameter(PARAMETER_ID_TEST);

        req.setAttribute(PARAMETER_ID_TEST,idTest);
        forwardToPage(PAGE_ADD_QUESTION, req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doPost add-question");

        String idTest = req.getParameter(PARAMETER_ID_TEST);

        String questionName = req.getParameter(PARAMETER_QUESTION_NAME);
        String questionIdTest = req.getParameter(PARAMETER_QUESTION_ID_TEST);

        try {
            Question question = new Question();
            Util.setQuestion(question, idTest, questionName);
            getServiceManager().getTutorService().addQuestion(question);
            sendRedirect("/tutor/edit-question-list?IdTest=" + idTest, resp);
        } catch (ParamException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
            req.setAttribute(PARAMETER_ID_TEST,idTest);
            forwardToPage(PAGE_ADD_QUESTION, req, resp);
        }
    }
}
