package it.source.tester.controller.tutor;

import it.source.tester.controller.AbstractController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

@WebServlet("/tutor/remove-test")
public class RemoveTestController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet remove-test");

        long idTest = Long.valueOf(req.getParameter(PARAMETER_ID_TEST));
        int res = getServiceManager().getTutorService().removeTest(idTest);
        if (res>0) {
            System.out.println("Test was deleted");
        } else {
            System.out.println("Test wasn't deleted");
        }
        sendRedirect("/tutor/edit-test-list", resp);

    }
}
