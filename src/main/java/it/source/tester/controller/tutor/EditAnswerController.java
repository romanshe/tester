package it.source.tester.controller.tutor;

import it.source.tester.Util.Util;
import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Answer;
import it.source.tester.exeption.ParamException;
import it.source.tester.exeption.ValidationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

@WebServlet("/tutor/edit-answer")
public class EditAnswerController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet edit-answer");

        String idTest = req.getParameter(PARAMETER_ID_TEST);
        String idQuestion = req.getParameter(PARAMETER_QUESTION_ID);
        long idAnswer = Long.valueOf(req.getParameter(PARAMETER_ANSWER_ID));

        try {
            Answer answer = getServiceManager().getTutorService().getAnswerById(idAnswer);
            req.setAttribute(ATTRIBUTE_ANSWER,answer);
        } catch (ValidationException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
            System.out.println(e.getMessage());
        }
        req.setAttribute(ATTRIBUTE_ID_TEST,idTest);
        req.setAttribute(ATTRIBUTE_QUESTION_ID,idQuestion);
        forwardToPage(PAGE_EDIT_ANSWER, req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doPost edit-answer");

        String idTest = req.getParameter(PARAMETER_ID_TEST);
        String idQuestion = req.getParameter(PARAMETER_QUESTION_ID);
        long idAnswer = Long.valueOf(req.getParameter(PARAMETER_ANSWER_ID));

        String answerName = req.getParameter(PARAMETER_ANSWER_NAME);
        String answerCorrect = req.getParameter(PARAMETER_ANSWER_CORRECT);

        try {
            Answer answer = getServiceManager().getTutorService().getAnswerById(idAnswer);
            Util.setAnswer(answer, idQuestion, answerName, answerCorrect);
            getServiceManager().getTutorService().updateAnswer(answer);
            sendRedirect("/tutor/edit-answer-list?IdTest=" + idTest+"&QUESTION_ID=" + idQuestion, resp);
        } catch (ValidationException | ParamException exc) {
            System.out.println(exc.getMessage());
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, exc.getMessage());
            req.setAttribute(ATTRIBUTE_ID_TEST,idTest);
            req.setAttribute(ATTRIBUTE_QUESTION_ID,idQuestion);
            forwardToPage(PAGE_EDIT_ANSWER, req, resp);
        }
    }
}
