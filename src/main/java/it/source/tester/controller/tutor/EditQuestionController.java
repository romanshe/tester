package it.source.tester.controller.tutor;

import it.source.tester.Util.Util;
import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Question;
import it.source.tester.exeption.ParamException;
import it.source.tester.exeption.ValidationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

@WebServlet("/tutor/edit-question")
public class EditQuestionController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet edit-question");

        long idQuestion = Long.valueOf(req.getParameter(PARAMETER_QUESTION_ID));
        String idTest =req.getParameter(PARAMETER_ID_TEST);

        try {
            Question question = getServiceManager().getTutorService().getQuestionById(idQuestion);
            req.setAttribute(ATTRIBUTE_QUESTION,question);
        } catch (ValidationException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
            System.out.println(e.getMessage());
        }

        req.setAttribute(PARAMETER_ID_TEST,idTest);
        req.setAttribute(ATTRIBUTE_QUESTION_ID,idQuestion);
        forwardToPage(PAGE_EDIT_QUESTION, req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doPost edit-question");

        String idTest = req.getParameter(PARAMETER_ID_TEST);
        long idQuestion = Long.valueOf(req.getParameter(PARAMETER_QUESTION_ID));

        String questionName = req.getParameter(PARAMETER_QUESTION_NAME);
        String questionIdTest = req.getParameter(PARAMETER_QUESTION_ID_TEST);

        try {
            Question question = getServiceManager().getTutorService().getQuestionById(idQuestion);
            Util.setQuestion(question, idTest, questionName);
            getServiceManager().getTutorService().updateQuestion(question);
            sendRedirect("/tutor/edit-question-list?IdTest=" + idTest, resp);
        } catch (ValidationException | ParamException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
            req.setAttribute(PARAMETER_ID_TEST,idTest);
            req.setAttribute(ATTRIBUTE_QUESTION_ID,idQuestion);
            forwardToPage(PAGE_EDIT_QUESTION, req, resp);
        }
    }
}
