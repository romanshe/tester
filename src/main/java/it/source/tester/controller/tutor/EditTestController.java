package it.source.tester.controller.tutor;

import it.source.tester.Util.Util;
import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Test;
import it.source.tester.exeption.ParamException;
import it.source.tester.exeption.ValidationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

@WebServlet("/tutor/edit-test")
public class EditTestController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet edit-test");

        long idTest = Long.valueOf(req.getParameter(PARAMETER_ID_TEST));
        try {
            Test test = getServiceManager().getTutorService().getTest(idTest);
            req.getSession().setAttribute(ATTRIBUTE_TEST, test);
        } catch (ValidationException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
        }
        forwardToPage(PAGE_EDIT_TEST, req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doPost edit-test");

        long idTest = Long.valueOf(req.getParameter(PARAMETER_ID_TEST));
        String testDescription = req.getParameter(PARAMETER_TEST_DESCRIPTION);
        String testName = req.getParameter(PARAMETER_TEST_NAME);
        String testDuration = req.getParameter(PARAMETER_TEST_DURATION);

        try {
            Test test = getServiceManager().getTutorService().getTest(idTest);
            Util.setTest(test, testName, testDescription, testDuration);
            getServiceManager().getTutorService().updateTest(test);
            System.out.println("Test" + test.getTestName() + " was successfully updated");
            sendRedirect("/tutor/edit-test?IdTest=" + idTest, resp);
        } catch (ValidationException | ParamException exception) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, exception.getMessage());
            forwardToPage(PAGE_EDIT_TEST, req, resp);
        }

    }
}
