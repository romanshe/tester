package it.source.tester.controller.tutor;

import it.source.tester.Util.Util;
import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Answer;
import it.source.tester.exeption.ParamException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

@WebServlet("/tutor/add-answer")
public class AddAnswerController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet add-answer");

        String idTest = req.getParameter(PARAMETER_ID_TEST);
        String idQuestion = req.getParameter(PARAMETER_QUESTION_ID);

        req.setAttribute(ATTRIBUTE_ID_TEST,idTest);
        req.setAttribute(ATTRIBUTE_QUESTION_ID,idQuestion);
        forwardToPage(PAGE_ADD_ANSWER, req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doPost add-answer");

        String idTest = req.getParameter(PARAMETER_ID_TEST);
        String idQuestion = req.getParameter(PARAMETER_QUESTION_ID);

        String answerName = req.getParameter(PARAMETER_ANSWER_NAME);
        String answerCorrect = req.getParameter(PARAMETER_ANSWER_CORRECT);

        try {
            Answer answer = new Answer();
            Util.setAnswer(answer, idQuestion, answerName, answerCorrect);
            getServiceManager().getTutorService().addAnswer(answer);
            sendRedirect("/tutor/edit-answer-list?IdTest=" + idTest+"&QUESTION_ID=" + idQuestion, resp);
        } catch (ParamException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
            req.setAttribute(ATTRIBUTE_ID_TEST,idTest);
            req.setAttribute(ATTRIBUTE_QUESTION_ID,idQuestion);
            forwardToPage(PAGE_ADD_ANSWER, req, resp);
        }
    }
}
