package it.source.tester.controller.tutor;

import it.source.tester.controller.AbstractController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

@WebServlet("/tutor/remove-answer")
public class RemoveAnswerController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet remove-answer");

        long idQuestion = Long.valueOf(req.getParameter(PARAMETER_QUESTION_ID));
        long idTest = Long.valueOf(req.getParameter(PARAMETER_ID_TEST));
        long idAnswer = Long.valueOf(req.getParameter(PARAMETER_ANSWER_ID));

        int res = getServiceManager().getTutorService().removeAnswer(idAnswer);
        if (res>0) {
            System.out.println("Answer was deleted");
        } else {
            System.out.println("Answer wasn't deleted");
        }
        sendRedirect("/tutor/edit-answer-list?IdTest=" + idTest + "&QUESTION_ID=" + idQuestion, resp);
    }
}
