package it.source.tester.controller.tutor;

import it.source.tester.controller.AbstractController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

@WebServlet("/tutor/remove-question")
public class RemoveQuestionController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet remove-question");

        long idQuestion = Long.valueOf(req.getParameter(PARAMETER_QUESTION_ID));
        long idTest = Long.valueOf(req.getParameter(PARAMETER_ID_TEST));
        int res = getServiceManager().getTutorService().removeQuestion(idQuestion);
        if (res>0) {
            System.out.println("Question was deleted");
        } else {
            System.out.println("Question wasn't deleted");
        }
        sendRedirect("/tutor/edit-question-list?IdTest=" + idTest, resp);
    }
}
