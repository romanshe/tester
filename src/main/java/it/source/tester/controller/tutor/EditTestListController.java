package it.source.tester.controller.tutor;

import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Account;
import it.source.tester.entity.Test;
import it.source.tester.exeption.ValidationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static it.source.tester.Constants.*;

@WebServlet("/tutor/edit-test-list")
public class EditTestListController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet edit-test-list");

        Account account = (Account) req.getSession().getAttribute(ATTRIBUTE_CURRENT_ACCOUNT);
        Long roleId = (Long) req.getSession().getAttribute(ATTRIBUTE_CURRENT_ROLE_ID);
        try {
            List<Test> testList;
            if (roleId == TUTOR_ROLE_ID){
                testList = getServiceManager().getUserService().getTestList(account.getId());
            } else {
                testList = getServiceManager().getUserService().getTestList();
            }
            req.setAttribute(ATTRIBUTE_TEST_LIST, testList);
        } catch (ValidationException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
        }
        forwardToPage(PAGE_EDIT_TEST_LIST, req, resp);
    }
}
