package it.source.tester.controller.tutor;

import it.source.tester.controller.AbstractController;
import it.source.tester.entity.Answer;
import it.source.tester.exeption.ValidationException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static it.source.tester.Constants.*;

@WebServlet("/tutor/edit-answer-list")
public class EditAnswerListController extends AbstractController {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doGet edit-answer-list");

        String idTest = req.getParameter(PARAMETER_ID_TEST);
        long idQuestion = Long.valueOf(req.getParameter(PARAMETER_QUESTION_ID));

        try {
            List<Answer> answerList = getServiceManager().getUserService().getAnswerForQuestion(idQuestion);
            req.setAttribute(ATTRIBUTE_ANSWER_LIST, answerList);
        } catch (ValidationException e) {
            req.setAttribute(ATTRIBUTE_ERROR_MESSAGE, e.getMessage());
        }
        req.setAttribute(ATTRIBUTE_ID_TEST,idTest);
        req.setAttribute(ATTRIBUTE_QUESTION_ID,idQuestion);

        forwardToPage(PAGE_EDIT_ANSWER_LIST, req, resp);
    }

}
