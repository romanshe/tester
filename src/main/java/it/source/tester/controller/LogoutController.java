package it.source.tester.controller;

import it.source.tester.Util.CookieUtil;
import it.source.tester.exeption.CookieException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

@WebServlet("/logout")
public class LogoutController extends AbstractController {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.getSession().setAttribute(ATTRIBUTE_CURRENT_ACCOUNT, null);
        req.getSession().setAttribute(ATTRIBUTE_CURRENT_ROLE_ID, null);
        req.getSession().invalidate();
        try {
            CookieUtil.deleteCookie(COOKIE_REMEMBER_ME_ID, req, resp);
            CookieUtil.deleteCookie(COOKIE_REMEMBER_ME_ROLE_ID, req, resp);
        } catch (CookieException e) {
        }
        sendRedirect("/login", resp);
    }
}
