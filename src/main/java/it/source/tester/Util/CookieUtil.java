package it.source.tester.Util;

import it.source.tester.exeption.CookieException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;


public class CookieUtil {

    public static Cookie getCookie(String cookieName, HttpServletRequest req) {
        List<Cookie> cookies = Arrays.asList(req.getCookies());
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(cookieName)) {
                return cookie;
            }
        }
        return null;
    }

    public static String getCookieValue(String cookieName, HttpServletRequest req) throws CookieException {
        Cookie cookie = getCookie(cookieName, req);
        if (cookie != null ) {
            return cookie.getValue();
        } else {
            throw new CookieException("No such cookie was find");
        }
    }

    public static void addCookie(String cookieName, String value, HttpServletResponse resp) {
        Cookie cookie = new Cookie(cookieName, value);
        cookie.setPath("/");
        cookie.setMaxAge(60 * 60 * 24 * 30); // valid for 30 days
        resp.addCookie(cookie);
    }

    public static void deleteCookie(String cookieName, HttpServletRequest req, HttpServletResponse resp) throws CookieException {
        Cookie cookie = getCookie(cookieName, req);
        if (cookie == null){
            throw new CookieException("No such cookie was find");
        } else {
            cookie.setPath("/");
            cookie.setMaxAge(0);
            resp.addCookie(cookie);
        }
    }
}
