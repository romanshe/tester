package it.source.tester.Util;

import it.source.tester.entity.*;
import it.source.tester.exeption.ParamException;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static it.source.tester.Constants.*;

public class Util {

    public static void setAccount(Account account, String  login, String  password, String firstName, String lastName, String middleName, String email) throws ParamException{
        if (StringUtils.isNotEmpty(login)) {
            account.setLogin(login);
        } else {
            throw new ParamException("Missed required param: account login");
        }
        if (StringUtils.isNotEmpty(password)) {
            account.setPassword(password);
        } else {
            throw new ParamException("Missed required param: account password");
        }
        if (StringUtils.isNotEmpty(firstName)) {
            account.setFirstName(firstName);
        } else {
            throw new ParamException("Missed required param: account first name");
        }
        if (StringUtils.isNotEmpty(lastName)){
            account.setLastName(lastName);
        } else {
            throw new ParamException("Missed required param: account last name");
        }
        if (StringUtils.isNotEmpty(middleName)) {
            account.setMiddleName(middleName);
        } else {
            throw new ParamException("Missed required param: account middle name");
        }
        if (StringUtils.isNotEmpty(email)) {
            account.setEmail(email);
        } else {
            throw new ParamException("Missed required param: account email");
        }
    }


    public static void setTest(Test test,String testName, String testDescription, String testDuration) throws ParamException{

        if (StringUtils.isNotEmpty(testDescription)) {
            test.setDescription(testDescription);
        } else {
            throw new ParamException("Missed required param: test description");
        }
        if (StringUtils.isNotEmpty(testName)) {
            test.setTestName(testName);
        } else {
            throw new ParamException("Missed required param: test name");
        }
        if (StringUtils.isNotEmpty(testDuration)) {
            test.setDuration_per_question(Short.parseShort(testDuration));
        } else {
            throw new ParamException("Missed required param: test duration");
        }
    }

    public static void setAnswer(Answer answer, String idQuestion, String answerName, String answerCorrect) throws ParamException {
        if (StringUtils.isNotEmpty(idQuestion)){
            answer.setId_question(Long.parseLong(idQuestion));
        } else {
            throw new ParamException("Missed required param: id question");
        }

        if (StringUtils.isNotEmpty(answerName) && !answerName.equals(answer.getName())) {
            answer.setName(answerName);
        } else {
            throw new ParamException("Missed required param: answer name");
        }

        if (StringUtils.isNotEmpty(answerCorrect)) {
            answer.setCorrect(answerCorrect.equals("on"));
        } else {
            answer.setCorrect(false);
        }
    }

    public static void setQuestion(Question question, String idTest, String questionName) throws ParamException {
        if (StringUtils.isNotEmpty(idTest)){
            question.setId_test(Long.parseLong(idTest));
        } else {
            throw new ParamException("Missed required param: id test");
        }

        if (StringUtils.isNotEmpty(questionName) && !questionName.equals(question.getQuestion())) {
            question.setQuestion(questionName);
        } else {
            throw new ParamException("Missed required param: question name");
        }
    }

    public static Result setResult(Test test, Account account, double studentResult){
        Result result = new Result();
        result.setId_test(test.getId());
        result.setTest_name(test.getTestName());
        result.setId_account(account.getId());
        result.setPercent(studentResult);
        return result;
    }

    public static List<Role> setRoles(String roleAdmin,String roleAdvancedTutor, String roleTutor, String roleStudent){
        List<Role> roles = new ArrayList();
        if (StringUtils.isNotEmpty(roleAdmin) && roleAdmin.equals("on")) {
            roles.add(ADMIN_ROLE);
        }
        if (StringUtils.isNotEmpty(roleAdvancedTutor) && roleAdvancedTutor.equals("on")) {
            roles.add(ADVANCED_TUTOR_ROLE);
        }
        if (StringUtils.isNotEmpty(roleTutor) && roleTutor.equals("on")) {
            roles.add(TUTOR_ROLE);
        }
        if (StringUtils.isNotEmpty(roleStudent) && roleStudent.equals("on")) {
            roles.add(STUDENT_ROLE);
        }
        return roles;
    }

    public static void loadProperties(Properties props, String classPathUrl)
    {
        try (InputStream in = Util.class.getClassLoader().getResourceAsStream(classPathUrl))
        {
            props.load(in);
        }
        catch (IOException e)
        {
            throw new IllegalArgumentException("Can't load properties from classpath:" + classPathUrl, e);
        }
    }

}
