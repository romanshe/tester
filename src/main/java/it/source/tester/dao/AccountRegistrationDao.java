package it.source.tester.dao;

public interface AccountRegistrationDao {
    Long getIdAccountByToken(String token);
    int addToken(Long idAccount, String token);
}
