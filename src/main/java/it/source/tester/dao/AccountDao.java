package it.source.tester.dao;

import it.source.tester.entity.Account;

import java.util.List;

public interface AccountDao {

    Account findById(long idAccount);
    Account findByLogin(String login);
    Account findByEmail(String email);
    int removeById(long idAccount);
    int setStatusById(long idAccount, boolean status);
    int updateAccount(Account account);
    int addAccount(Account account);
    List<Account> findAll();
}
