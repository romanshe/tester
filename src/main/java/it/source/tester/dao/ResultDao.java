package it.source.tester.dao;

import it.source.tester.entity.Result;

import java.util.List;

public interface ResultDao {

    List<Result> findByIdAccount(Long idAccount);
    Result findById(Long idResult);
    int addResult(Result result);
}
