package it.source.tester.dao.impl;

import it.source.tester.dao.AccountRoleDao;
import it.source.tester.entity.Account;
import it.source.tester.entity.Role;
import it.source.tester.exeption.ApplicationException;
import it.source.tester.factory.ConnectionUtils;
import org.apache.commons.dbutils.QueryRunner;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;


public class AccountRoleDaoImpl implements AccountRoleDao {

    private final QueryRunner queryRunner = new QueryRunner();

    @Override
    public int addRolesForAccount(Long accountId, List<Role> roles) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        int result = 0;
        try {
            for (Role role: roles) {
                result += queryRunner.update(connection,"INSERT INTO account_role values(?, ?, nextval('account_role_seq'))",
                        accountId, role.getId());
            }
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
        return result;
    }

    @Override
    public int updateRolesForAccount(Long accountId, List<Role> roles) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        int result = 0;
        try {
            for (Role role: roles) {
                result += queryRunner.update(connection,"UPDATE account_role SET id_role=? WHERE id_account=?",
                        role.getId(), accountId);
            }
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
        return result;
    }

    @Override
    public int deleteRolesForAccount(Long accountId, List<Role> roles) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        int result = 0;
        try {
            for (Role role: roles) {
                result += queryRunner.update(connection,"DELETE  FROM account_role WHERE id_account=? and id_role=?",
                        accountId, role.getId());
            }
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
        return result;
    }
}


