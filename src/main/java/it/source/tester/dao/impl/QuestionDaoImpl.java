package it.source.tester.dao.impl;

import it.source.tester.dao.QuestionDao;
import it.source.tester.entity.Question;
import it.source.tester.exeption.ApplicationException;
import it.source.tester.factory.ConnectionUtils;
import it.source.tester.handler.DefaultListResultSetHandler;
import it.source.tester.handler.DefaultOneRowResultSetHandler;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static it.source.tester.factory.ResultSetHandlerFactory.QUESTION_RESULT_SET_HANDLER;

public class QuestionDaoImpl implements QuestionDao {

    private final QueryRunner queryRunner = new QueryRunner();
    private ResultSetHandler<List<Question>> listQuestionHandler = new DefaultListResultSetHandler(QUESTION_RESULT_SET_HANDLER);
    private ResultSetHandler<Question> rowQuestionHandler = new DefaultOneRowResultSetHandler(QUESTION_RESULT_SET_HANDLER);


    @Override
    public List<Question> findByIdTest(long idTest) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.query(connection, "SELECT * FROM question where id_test=?", listQuestionHandler, idTest );
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public int addQuestion(Question question) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.update(connection,"INSERT INTO question values(nextval('question_seq'), ?, ?)",
                    question.getId_test(), question.getQuestion());
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public int updateQuestion(Question question) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.update(connection,"UPDATE question SET id_test=?, question=?  WHERE id=?",
                    question.getId_test(), question.getQuestion(), question.getId());
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public int removeQuestion(Long idQuestion) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.update(connection,"DELETE FROM question WHERE id=?", idQuestion);
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public Question findById(long idQuestion) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.query(connection, "SELECT * FROM question where id=?", rowQuestionHandler, idQuestion );
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }
}
