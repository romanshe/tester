package it.source.tester.dao.impl;

import it.source.tester.dao.AnswerDao;
import it.source.tester.entity.Answer;
import it.source.tester.exeption.ApplicationException;
import it.source.tester.factory.ConnectionUtils;
import it.source.tester.handler.DefaultListResultSetHandler;
import it.source.tester.handler.DefaultOneRowResultSetHandler;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static it.source.tester.factory.ResultSetHandlerFactory.*;

public class AnswerDaoImpl implements AnswerDao {

    private final QueryRunner queryRunner = new QueryRunner();
    private ResultSetHandler<List<Answer>> listAnswerHandler = new DefaultListResultSetHandler(ANSWER_RESULT_SET_HANDLER);
    private ResultSetHandler<Answer> rowAnswerHandler = new DefaultOneRowResultSetHandler(ANSWER_RESULT_SET_HANDLER);


    @Override
    public List<Answer> findByIdQuestion(long idQuestion) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.query(connection, "SELECT * FROM answer where id_question=?", listAnswerHandler, idQuestion );
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public int addAnswer(Answer answer) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.update(connection,"INSERT INTO answer values(nextval('answer_seq'), ?, ?, ?)",
                    answer.getId_question(), answer.getName(), answer.isCorrect());
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public int updateAnswer(Answer answer) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.update(connection,"UPDATE answer SET id_question=?, name=?, correct=? WHERE id=?",
                    answer.getId_question(), answer.getName(), answer.isCorrect(), answer.getId());
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public int removeAnswer(Long idAnswer) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.update(connection,"DELETE FROM answer WHERE id=?", idAnswer);
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public Answer findById(long idAnswer) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.query(connection, "SELECT * FROM answer where id=?", rowAnswerHandler, idAnswer );
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }
}
