package it.source.tester.dao.impl;

import it.source.tester.dao.AccountRegistrationDao;
import it.source.tester.entity.Answer;
import it.source.tester.exeption.ApplicationException;
import it.source.tester.factory.ConnectionUtils;
import it.source.tester.handler.DefaultOneRowResultSetHandler;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.Connection;
import java.sql.SQLException;

import static it.source.tester.factory.ResultSetHandlerFactory.ACCOUNT_ID_BY_TOKEN_SET_HANDLER;

public class AccountRegistrationDaoImpl implements AccountRegistrationDao {
    private final QueryRunner queryRunner = new QueryRunner();
    private ResultSetHandler<Long> rowAccountIdByTokenHandler = new DefaultOneRowResultSetHandler(ACCOUNT_ID_BY_TOKEN_SET_HANDLER);

    @Override
    public Long getIdAccountByToken(String token) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.query(connection, "SELECT id_account FROM account_registration where token=?",rowAccountIdByTokenHandler, token);
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public int addToken(Long idAccount, String token) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            int result = queryRunner.update(connection, "INSERT INTO account_registration values(?, ?, now())", idAccount, token);
            return result;
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }
}
