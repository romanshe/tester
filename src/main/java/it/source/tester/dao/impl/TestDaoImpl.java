package it.source.tester.dao.impl;

import it.source.tester.dao.TestDao;
import it.source.tester.entity.Test;
import it.source.tester.exeption.ApplicationException;
import it.source.tester.factory.ConnectionUtils;
import it.source.tester.handler.DefaultListResultSetHandler;
import it.source.tester.handler.DefaultOneRowResultSetHandler;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static it.source.tester.factory.ResultSetHandlerFactory.*;

public class TestDaoImpl implements TestDao {

    private final QueryRunner queryRunner = new QueryRunner();
    private ResultSetHandler<List<Test>> listTestHandler = new DefaultListResultSetHandler(TEST_WITH_AUTHOR_RESULT_SET_HANDLER);
    private ResultSetHandler<Test> rowTestHandler = new DefaultOneRowResultSetHandler(TEST_RESULT_SET_HANDLER);


    @Override
    public List<Test> findAll() {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.query(connection, "SELECT test.*, account.first_name, account.last_name FROM test, account WHERE test.id_account=account.id", listTestHandler);
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public Test findById(Long idTest) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.query(connection, "SELECT * FROM test where id=?", rowTestHandler, idTest );
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public List<Test> findByAccountId(long accountId) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.query(connection, "SELECT test.*, account.first_name, account.last_name FROM test, account WHERE test.id_account=account.id and id_account=?", listTestHandler, accountId );
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public int addTest(Test test) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.update(connection,"INSERT INTO test values(nextval('test_seq'), ?, ?, ?, ?)",
                    test.getTestName(), test.getDuration_per_question(),test.getId_account(), test.getDescription() );
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public int updateTest(Test test) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.update(connection,"UPDATE test SET test_name=?, duration_per_question=?, id_account=?, description=?  WHERE id=?",
                    test.getTestName(), test.getDuration_per_question(), test.getId_account(), test.getDescription(), test.getId());
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public int removeTest(Long idTest) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.update(connection,"DELETE FROM test WHERE id=?", idTest);
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public Test findByName(String testName) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.query(connection, "SELECT * FROM test where test_name=?", rowTestHandler, testName );
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }
}
