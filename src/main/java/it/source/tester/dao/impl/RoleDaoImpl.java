package it.source.tester.dao.impl;

import it.source.tester.dao.RoleDao;
import it.source.tester.entity.Role;
import it.source.tester.exeption.ApplicationException;
import it.source.tester.factory.ConnectionUtils;
import it.source.tester.handler.DefaultListResultSetHandler;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;


import static it.source.tester.factory.ResultSetHandlerFactory.*;

public class RoleDaoImpl implements RoleDao {

    private final QueryRunner queryRunner = new QueryRunner();
    private ResultSetHandler<List<Role>> listRowHandler = new DefaultListResultSetHandler(ROLE_RESULT_SET_HANDLER);

    @Override
    public List<Role> findRoleById(long userId) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.query(connection, "SELECT r.id, r.name FROM role as r, account_role as ar where r.id = ar.id_role and ar.id_account=?", listRowHandler, userId );
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }
}
