package it.source.tester.dao.impl;

import it.source.tester.dao.AccountDao;
import it.source.tester.entity.Account;
import it.source.tester.exeption.ApplicationException;
import it.source.tester.factory.ConnectionUtils;
import it.source.tester.handler.*;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static it.source.tester.factory.ResultSetHandlerFactory.ACCOUNT_RESULT_SET_HANDLER;

public class AccountDaoImpl implements AccountDao {

    private final QueryRunner queryRunner = new QueryRunner();
    private ResultSetHandler<Account> rowAccountHandler = new DefaultOneRowResultSetHandler(ACCOUNT_RESULT_SET_HANDLER);
    private ResultSetHandler<List<Account>> listAccountHandler = new DefaultListResultSetHandler(ACCOUNT_RESULT_SET_HANDLER);

    @Override
    public Account findById(long idAccount) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.query(connection, "SELECT * FROM account where id=?", rowAccountHandler, idAccount);
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public Account findByLogin(String login) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.query(connection, "SELECT * FROM account where login=?", rowAccountHandler, login);
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public List<Account> findAll() {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.query(connection, "SELECT * FROM account ORDER BY created DESC", listAccountHandler);
        } catch (SQLException e) {
            throw new ApplicationException(e);
        }
    }

    @Override
    public int removeById(long idAccount) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.update(connection,"DELETE FROM account WHERE id=?", idAccount);
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public int setStatusById(long idAccount, boolean status) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.update(connection,"UPDATE account SET active=?  WHERE id=?",status, idAccount);
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public int updateAccount(Account account) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.update(connection,
                    "UPDATE account SET first_name=?, last_name=?, middle_name=?, email=?, login=?, password=? where id=?",
                    account.getFirstName(), account.getLastName(), account.getMiddleName(), account.getEmail(), account.getLogin(), account.getPassword(), account.getId());
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public int addAccount(Account account) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.update(connection,"INSERT INTO account values(nextval('account_seq'), ?, ?, ?, false, ?, ?, ?, now())",
                    account.getFirstName(), account.getLastName(), account.getEmail(), account.getPassword(), account.getLogin(), account.getMiddleName());
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public Account findByEmail(String email) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.query(connection, "SELECT * FROM account where email=?", rowAccountHandler, email);
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }
}
