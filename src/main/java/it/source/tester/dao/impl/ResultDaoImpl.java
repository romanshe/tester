package it.source.tester.dao.impl;

import it.source.tester.dao.ResultDao;
import it.source.tester.entity.Result;
import it.source.tester.exeption.ApplicationException;
import it.source.tester.factory.ConnectionUtils;
import it.source.tester.handler.DefaultListResultSetHandler;
import it.source.tester.handler.DefaultOneRowResultSetHandler;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static it.source.tester.factory.ResultSetHandlerFactory.*;

public class ResultDaoImpl implements ResultDao {
    private final QueryRunner queryRunner = new QueryRunner();
    private ResultSetHandler<Result> rowResultHandler = new DefaultOneRowResultSetHandler(RESULT_RESULT_SET_HANDLER);
    private ResultSetHandler<List<Result>> listResultHandler = new DefaultListResultSetHandler(RESULT_RESULT_SET_HANDLER);


    @Override
    public List<Result> findByIdAccount(Long idAccount) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.query(connection, "SELECT * FROM result  WHERE id_account=?", listResultHandler, idAccount);
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }

    @Override
    public Result findById(Long idResult) {
        return null;
    }

    @Override
    public int addResult(Result result) {
        Connection connection = ConnectionUtils.getCurrentConnection();
        try {
            return queryRunner.update(connection, "INSERT into result values(?, ?, ?, nextval('result_seq'), ?, now())",
                    result.getId_test(), result.getPercent(), result.getId_account(), result.getTest_name());
        } catch (SQLException e) {
            throw new ApplicationException(e.getMessage(), e);
        }
    }
}
