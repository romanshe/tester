package it.source.tester.dao;

import it.source.tester.entity.Test;

import java.util.List;

public interface TestDao {

    List<Test> findAll();
    Test findById(Long idTest);
    Test findByName(String testName);
    List<Test> findByAccountId(long accountId);
    int addTest(Test test);
    int updateTest(Test test);
    int removeTest(Long idTest);
}

