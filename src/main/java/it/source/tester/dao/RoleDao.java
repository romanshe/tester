package it.source.tester.dao;

import it.source.tester.entity.Role;
import java.util.List;

public interface RoleDao {

    List<Role> findRoleById(long userId);
}
