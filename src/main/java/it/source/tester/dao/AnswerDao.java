package it.source.tester.dao;

import it.source.tester.entity.Answer;
import it.source.tester.entity.Question;

import java.util.List;

public interface AnswerDao {

    List<Answer> findByIdQuestion(long idQuestion);
    int addAnswer(Answer answer);
    int updateAnswer(Answer answer);
    int removeAnswer(Long idAnswer);
    Answer findById(long idAnswer);
}
