package it.source.tester.dao;

import it.source.tester.entity.Question;

import java.util.List;

public interface QuestionDao {

    List<Question> findByIdTest(long idTest);
    int addQuestion(Question question);
    int updateQuestion(Question question);
    int removeQuestion(Long idQuestion);
    Question findById(long idQuestion);
}
