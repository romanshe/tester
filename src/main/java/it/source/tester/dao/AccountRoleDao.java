package it.source.tester.dao;

import it.source.tester.entity.Account;
import it.source.tester.entity.Role;
import java.util.*;


public interface AccountRoleDao {
    int addRolesForAccount(Long accountId, List<Role> roles);
    int updateRolesForAccount(Long accountId, List<Role> roles);
    int deleteRolesForAccount(Long accountId, List<Role> roles);
}
