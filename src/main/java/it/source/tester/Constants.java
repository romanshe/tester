package it.source.tester;

import it.source.tester.entity.Role;

public class Constants {

    // WebApp context path
    public static final String APP_CONTEXT_PATH = "/tester";
    public static final String SESSION_LOCALE = "SESSION_LOCALE";

    // Parameters
    public static final String PARAMETER_LOGIN =        "LOGIN";
    public static final String PARAMETER_PASSWORD =     "PASSWORD";
    public static final String PARAMETER_FIRST_NAME =   "FIRST_NAME";
    public static final String PARAMETER_LAST_NAME =    "LAST_NAME";
    public static final String PARAMETER_MIDDLE_NAME =  "MIDDLE_NAME";
    public static final String PARAMETER_EMAIL =        "EMAIL";
    public static final String PARAMETER_ROLE =         "ROLE";
    public static final String PARAMETER_REMEMBER_ME =  "REMEMBER_ME";
    public static final String PARAMETER_ID_TEST =      "IdTest";
    public static final String PARAMETER_ID_ACCOUNT =    "idAccount";
    public static final String PARAMETER_TEST_RESET =    "testReset";
    public static final String PARAMETER_TEST_DIRECTION =   "direction";
    public static final String PARAMETER_TEST_DESCRIPTION = "TEST_DESCRIPTION";
    public static final String PARAMETER_TEST_NAME =        "TEST_NAME";
    public static final String PARAMETER_TEST_DURATION =    "TEST_DURATION";
    public static final String PARAMETER_QUESTION_NAME =    "QUESTION_NAME";
    public static final String PARAMETER_QUESTION_ID_TEST = "QUESTION_ID_TEST";
    public static final String PARAMETER_QUESTION_ID =      "QUESTION_ID";
    public static final String PARAMETER_ANSWER_NAME =      "ANSWER_NAME";
    public static final String PARAMETER_ANSWER_ID =        "ANSWER_ID";
    public static final String PARAMETER_ANSWER_ID_TEST =   "ANSWER_ID_TEST";
    public static final String PARAMETER_ANSWER_CORRECT =   "ANSWER_CORRECT";
    public static final String PARAMETER_ANSWER_CHECKBOX_ID =   "ANSWER_CHECKBOX_";
    public static final String PARAMETER_ADMIN_ROLE =      "ADMIN_ROLE";
    public static final String PARAMETER_ADVANCED_TUTOR_ROLE =      "ADVANCED_TUTOR_ROLE";
    public static final String PARAMETER_TUTOR_ROLE =       "TUTOR_ROLE";
    public static final String PARAMETER_STUDENT_ROLE =     "STUDENT_ROLE";
    public static final String PARAMETER_FINISH_BUTTON = "FINISH_BUTTON";
    public static final String PARAMETER_LANG = "LANG";
    public static final String PARAMETER_UUID = "UUID";

    // Attribute
    public static final String ATTRIBUTE_ID_TEST =              "IdTest";
    public static final String ATTRIBUTE_TESTER_SERVICE =       "TESTER_SERVICE";
    public static final String ATTRIBUTE_CURRENT_ACCOUNT =      "CURRENT_ACCOUNT";
    public static final String ATTRIBUTE_CURRENT_ROLE_ID =      "CURRENT_ROLE_ID";
    public static final String ATTRIBUTE_EDIT_ACCOUNT =         "EDIT_ACCOUNT";
    public static final String ATTRIBUTE_ERROR_MESSAGE =        "ERROR";
    public static final String ATTRIBUTE_TEST_LIST =            "TEST_LIST";
    public static final String ATTRIBUTE_TEST =                 "TEST";
    public static final String ATTRIBUTE_QUESTION_LIST =        "QUESTION_LIST";
    public static final String ATTRIBUTE_QUESTION =             "QUESTION";
    public static final String ATTRIBUTE_QUESTION_ID =          "QUESTION_ID";
    public static final String ATTRIBUTE_CURRENT_QUESTION_NUM = "CURRENT_QUESTION_NUM";
    public static final String ATTRIBUTE_CURRENT_QUESTION =     "CURRENT_QUESTION";
    public static final String ATTRIBUTE_ANSWER_LIST =          "ANSWER_LIST";
    public static final String ATTRIBUTE_CORRECT_ANSWER_LIST =  "CORRECT_ANSWER_LIST";
    public static final String ATTRIBUTE_ANSWER =               "ANSWER";
    public static final String ATTRIBUTE_ACCOUNT_LIST =         "ACCOUNT_LIST";
    public static final String ATTRIBUTE_TEST_RESULT =          "TEST_RESULT";
    public static final String ATTRIBUTE_RESULT_LIST =          "RESULT_LIST";
    public static final String ATTRIBUTE_ADMIN_ROLE =            "ADMIN_ROLE";
    public static final String ATTRIBUTE_ADVANCED_TUTOR_ROLE =   "ADVANCED_TUTOR_ROLE";
    public static final String ATTRIBUTE_TUTOR_ROLE =            "TUTOR_ROLE";
    public static final String ATTRIBUTE_STUDENT_ROLE =          "STUDENT_ROLE";

    // Pages -- common
    public static final String PAGE_TEMPLATE =          "/WEB-INF/view/page-template.jsp";
    public static final String PAGE_LOGIN =             "/WEB-INF/view/Login.jsp";
    public static final String PAGE_SIGN_UP =           "/WEB-INF/view/SignUp.jsp";
    public static final String PAGE_FORGET_PASSWORD =   "/WEB-INF/view/ForgetPassword.jsp";

    // Pages -- student
    public static final String PAGE_TEST_LIST =                 "/WEB-INF/view/student/TestList.jsp";
    public static final String PAGE_TEST_PAGE =                 "/WEB-INF/view/student/TestPage.jsp";
    public static final String PAGE_OFF_LINE_TEST_QUESTION =    "/WEB-INF/view/student/OfflineTestQuestion.jsp";
    public static final String PAGE_ON_LINE_TEST_QUESTION =     "/WEB-INF/view/student/OnlineTestQuestion.jsp";
    public static final String PAGE_TEST_RESULT =               "/WEB-INF/view/student/TestResult.jsp";
    public static final String PAGE_RESULT_LIST =               "/WEB-INF/view/student/ResultList.jsp";
    public static final String PAGE_MY_INFO =                   "/WEB-INF/view/MyInfo.jsp";

    // Pages -- tutor, advanced-tutor
    public static final String PAGE_EDIT_TEST_LIST =            "/WEB-INF/view/tutor/EditTestList.jsp";
    public static final String PAGE_EDIT_QUESTION_LIST =        "/WEB-INF/view/tutor/EditQuestionList.jsp";
    public static final String PAGE_EDIT_ANSWER_LIST =          "/WEB-INF/view/tutor/EditAnswerList.jsp";
    public static final String PAGE_ADD_TEST =                  "/WEB-INF/view/tutor/AddTest.jsp";
    public static final String PAGE_EDIT_TEST =                 "/WEB-INF/view/tutor/EditTest.jsp";
    public static final String PAGE_ADD_QUESTION =              "/WEB-INF/view/tutor/AddQuestion.jsp";
    public static final String PAGE_EDIT_QUESTION =              "/WEB-INF/view/tutor/EditQuestion.jsp";
    public static final String PAGE_ADD_ANSWER =                "/WEB-INF/view/tutor/AddAnswer.jsp";
    public static final String PAGE_EDIT_ANSWER =                "/WEB-INF/view/tutor/EditAnswer.jsp";

    // Pages -- admin
    public static final String PAGE_ACCOUNT_LIST =              "/WEB-INF/view/admin/AccountList.jsp";
    public static final String PAGE_EDIT_ACCOUNT =              "/WEB-INF/view/admin/AccountEdit.jsp";
    public static final String PAGE_ADD_ACCOUNT =               "/WEB-INF/view/admin/AccountAdd.jsp";

    //Misc

    public static final Integer FIRST_QUESTION_NUMBER = new Integer(0);
    public static final Integer CORRECT_ANSWER_VALUE = new Integer(1);
    public static final Integer NOT_CORRECT_ANSWER_VALUE = new Integer(0);

    //Role
    public static final Long STUDENT_ROLE_ID = 3L;
    public static final Long TUTOR_ROLE_ID = 2L;
    public static final Long ADVANCED_TUTOR_ROLE_ID = 1L;
    public static final Long ADMIN_ROLE_ID = 0L;

    public static final Role STUDENT_ROLE = new Role(STUDENT_ROLE_ID);
    public static final Role TUTOR_ROLE = new Role(TUTOR_ROLE_ID);
    public static final Role ADVANCED_TUTOR_ROLE = new Role(ADVANCED_TUTOR_ROLE_ID);
    public static final Role ADMIN_ROLE = new Role(ADMIN_ROLE_ID);


    //COOKIE
    public static final String COOKIE_REMEMBER_ME_ID = "remember-me-id";
    public static final String COOKIE_REMEMBER_ME_ROLE_ID = "remember-me-role-id";
}
