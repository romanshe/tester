package it.source.tester.factory;

import it.source.tester.entity.*;
import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.ResultSet;
import java.sql.SQLException;



public class ResultSetHandlerFactory {
    private ResultSetHandlerFactory(){

    }

    public static ResultSetHandler<Account> ACCOUNT_RESULT_SET_HANDLER = new ResultSetHandler<Account>() {
        @Override
        public Account handle(ResultSet resultSet) throws SQLException {
            Account result = new Account();
            result.setId(resultSet.getLong("id"));
            result.setLogin(resultSet.getString("login"));
            result.setPassword(resultSet.getString("password"));
            result.setEmail(resultSet.getString("email"));
            result.setFirstName(resultSet.getString("first_name"));
            result.setLastName(resultSet.getString("last_name"));
            result.setMiddleName(resultSet.getString("middle_name"));
            result.setStatus(resultSet.getBoolean("active"));
            return result;
        }
    };


    public static ResultSetHandler<Test> TEST_RESULT_SET_HANDLER = new ResultSetHandler<Test>() {
        @Override
        public Test handle(ResultSet resultSet) throws SQLException {
            Test result = new Test();
            result.setId(resultSet.getLong("id"));
            result.setTestName(resultSet.getString("test_name"));
            result.setDuration_per_question(resultSet.getShort("duration_per_question"));
            result.setId_account(resultSet.getLong("id_account"));
            result.setDescription(resultSet.getString("description"));
            return result;
        }
    };
    public static ResultSetHandler<Test> TEST_WITH_AUTHOR_RESULT_SET_HANDLER = new ResultSetHandler<Test>() {
        @Override
        public Test handle(ResultSet resultSet) throws SQLException {
            Test result = new Test();
            result.setId(resultSet.getLong("id"));
            result.setTestName(resultSet.getString("test_name"));
            result.setDuration_per_question(resultSet.getShort("duration_per_question"));
            result.setId_account(resultSet.getLong("id_account"));
            result.setDescription(resultSet.getString("description"));
            result.setAuthorName(resultSet.getString("first_name") + " " + resultSet.getString("last_name"));
            return result;
        }
    };

    public static ResultSetHandler<Question> QUESTION_RESULT_SET_HANDLER = new ResultSetHandler<Question>() {
        @Override
        public Question handle(ResultSet resultSet) throws SQLException {
            Question result = new Question();
            result.setId(resultSet.getLong("id"));
            result.setQuestion(resultSet.getString("question"));
            result.setId_test(resultSet.getLong("id_test"));
            return result;
        }
    };

    public static ResultSetHandler<Answer> ANSWER_RESULT_SET_HANDLER = new ResultSetHandler<Answer>() {
        @Override
        public Answer handle(ResultSet resultSet) throws SQLException {
            Answer result = new Answer();
            result.setId(resultSet.getLong("id"));
            result.setId_question(resultSet.getLong("id_question"));
            result.setName(resultSet.getString("name"));
            result.setCorrect(resultSet.getBoolean("correct"));
            return result;
        }
    };

    public static ResultSetHandler<Role> ROLE_RESULT_SET_HANDLER = new ResultSetHandler<Role>() {
        @Override
        public Role handle(ResultSet resultSet) throws SQLException {
            Role result = new Role();
            result.setId(resultSet.getLong("id"));
            result.setName(resultSet.getString("name"));
            return result;
        }
    };
    public static ResultSetHandler<Result> RESULT_RESULT_SET_HANDLER = new ResultSetHandler<Result>() {
        @Override
        public Result handle(ResultSet resultSet) throws SQLException {
            Result result = new Result();
            result.setId(resultSet.getLong("id"));
            result.setId_test(resultSet.getLong("id_test"));
            result.setPercent(resultSet.getDouble("percent"));
            result.setId_account(resultSet.getLong("id_account"));
            result.setTest_name(resultSet.getString("test_name"));
            result.setCreated(resultSet.getDate("created"));
            return result;
        }
    };

    public static ResultSetHandler<Long> ACCOUNT_ID_BY_TOKEN_SET_HANDLER = new ResultSetHandler<Long>() {
        @Override
        public Long handle(ResultSet resultSet) throws SQLException {
            return resultSet.getLong("id_account");
        }
    };
}
