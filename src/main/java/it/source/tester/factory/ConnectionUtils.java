package it.source.tester.factory;

import java.sql.Connection;

public class ConnectionUtils {

    private static final ThreadLocal<Connection> connections = new ThreadLocal<>();

    public static Connection getCurrentConnection(){
        return connections.get();
    }

    public static void setCurrentConnection(Connection connection){
        connections.set(connection);
    }

    public static void clearCurrentConnection(){
        connections.remove();
    }
}
