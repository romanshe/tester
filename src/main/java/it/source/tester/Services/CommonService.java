package it.source.tester.Services;

import it.source.tester.entity.Account;
import it.source.tester.exeption.ValidationException;

public interface CommonService {

    Account login(String login, String password) throws ValidationException;
}
