package it.source.tester.Services;

import java.util.Locale;


public interface I18nService
{
	String getMessage(String key, Locale sessionLocale);
}
