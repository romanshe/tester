package it.source.tester.Services;

import it.source.tester.entity.Account;
import it.source.tester.entity.Role;
import it.source.tester.exeption.ExistRecordException;
import it.source.tester.exeption.ValidationException;

import java.util.List;

public interface AdminService {

    List<Account> findAll();
    Account findById(long idAccount) throws ValidationException;
    Account findByLogin(String login) throws ValidationException;
    int updateAccount(Account account);
    int removeById(long idAccount);
    int setStatusById(long idAccount, boolean status);
    int addAccount(Account account) throws ExistRecordException;
    int addRoles(Long accountId, List<Role> roles);
    int updateRoles(Long accountId, List<Role> roles);
    int deleteRoles(Long accountId, List<Role> roles);
    int addToken(Long idAccount, String token);
    void accountConfirmation(String token);
}
