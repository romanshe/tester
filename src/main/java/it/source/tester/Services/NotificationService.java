package it.source.tester.Services;

public interface NotificationService {
    void sendNotificationMessage(String notificationAddress, String content);

    void close();
}
