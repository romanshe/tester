package it.source.tester.Services.impl;

import it.source.tester.Services.I18nService;

import java.util.Locale;
import java.util.ResourceBundle;


public class I18nServiceImpl implements I18nService
{
	@Override
	public String getMessage(String key, Locale sessionLocale)
	{
		Locale locale = null;
		if (sessionLocale == null)
		{
			locale = Locale.getDefault();
		}
		else
		{
			locale = sessionLocale;
		}
		ResourceBundle rb = ResourceBundle.getBundle("messages", locale);
		return rb.getString(key);
	}
}
