package it.source.tester.Services.impl;


import it.source.tester.Services.TutorService;
import it.source.tester.dao.AnswerDao;
import it.source.tester.dao.QuestionDao;
import it.source.tester.dao.TestDao;
import it.source.tester.entity.Answer;
import it.source.tester.entity.Question;
import it.source.tester.entity.Test;
import it.source.tester.exeption.ApplicationException;
import it.source.tester.exeption.ExistRecordException;
import it.source.tester.exeption.ValidationException;
import it.source.tester.factory.ConnectionUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class TutorServiceImpl extends UserServiceImpl implements TutorService {

    public TutorServiceImpl(DataSource dataSource, TestDao testDao, QuestionDao questionDao, AnswerDao answerDao) {
        super(dataSource, testDao, questionDao, answerDao, null);
    }


    @Override
    public int addTest(Test test) throws ExistRecordException {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            if (testDao.findByName(test.getTestName()) != null){
                throw new ExistRecordException("Test with such name already exist.");
            }
            return testDao.addTest(test);
        } catch (SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int updateTest(Test test) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            return testDao.updateTest(test);
        } catch (SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int removeTest(Long idTest) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            return testDao.removeTest(idTest);
        } catch (SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int addQuestion(Question question) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            return questionDao.addQuestion(question);
        } catch (SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int updateQuestion(Question question) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            return questionDao.updateQuestion(question);
        } catch (SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int removeQuestion(Long idQuestion) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            return questionDao.removeQuestion(idQuestion);
        } catch (SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public Question getQuestionById(long idQuestion) throws ValidationException {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            Question question = questionDao.findById(idQuestion);
            if (question == null) {
                throw new ValidationException("No question was found ");
            }
            return question;
        } catch (SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int addAnswer(Answer answer) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            return answerDao.addAnswer(answer);
        } catch (SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int updateAnswer(Answer answer) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            return answerDao.updateAnswer(answer);
        } catch (SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int removeAnswer(Long idAnswer) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            return answerDao.removeAnswer(idAnswer);
        } catch (SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public Answer getAnswerById(long idAnswer) throws ValidationException {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            Answer answer = answerDao.findById(idAnswer);
            if (answer == null) {
                throw new ValidationException("No answer was found ");
            }
            return answer;
        } catch (SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }


}
