package it.source.tester.Services.impl;

import it.source.tester.Services.AdminService;
import it.source.tester.dao.AccountDao;
import it.source.tester.dao.AccountRegistrationDao;
import it.source.tester.dao.AccountRoleDao;
import it.source.tester.dao.RoleDao;
import it.source.tester.entity.Account;
import it.source.tester.entity.Role;
import it.source.tester.exeption.ApplicationException;
import it.source.tester.exeption.ExistRecordException;
import it.source.tester.exeption.ValidationException;
import it.source.tester.factory.ConnectionUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class AdminServiceImpl implements AdminService {

    private final AccountDao accountDao;
    private final DataSource dataSource;
    private final RoleDao roleDao;
    private final AccountRoleDao accountRoleDao;
    private final AccountRegistrationDao accountRegistrationDao;

    public AdminServiceImpl(DataSource dataSource, AccountDao accountDao, RoleDao roleDao, AccountRoleDao accountRoleDao, AccountRegistrationDao accountRegistrationDao) {
        this.accountDao = accountDao;
        this.dataSource = dataSource;
        this.roleDao = roleDao;
        this.accountRoleDao = accountRoleDao;
        this.accountRegistrationDao = accountRegistrationDao;

    }

    @Override
    public List<Account> findAll() {
        try (Connection connection = dataSource.getConnection();) {
            ConnectionUtils.setCurrentConnection(connection);
            List<Account> accountList = accountDao.findAll();
            for (Account account : accountList) {
                account.setRoles(roleDao.findRoleById(account.getId()));
            }
            return accountList;
        } catch ( SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public Account findById(long idAccount) throws ValidationException {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            Account account = accountDao.findById(idAccount);
            if (account == null) {
                throw new ValidationException("No account with such Id was found ");
            }
            account.setRoles(roleDao.findRoleById(account.getId()));
            return account;
        } catch ( SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }
    @Override
    public Account findByLogin(String login) throws ValidationException {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            Account account = accountDao.findByLogin(login);
            if (account == null) {
                throw new ValidationException("No account with such login was found ");
            }
            account.setRoles(roleDao.findRoleById(account.getId()));
            return account;
        } catch ( SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int removeById(long idAccount) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            return accountDao.removeById(idAccount);
        } catch ( SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int setStatusById(long idAccount, boolean status) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            return accountDao.setStatusById(idAccount,status);
        } catch ( SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int updateAccount(Account account) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            return accountDao.updateAccount(account);
        } catch ( SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int addAccount(Account account) throws ExistRecordException {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            if (accountDao.findByEmail(account.getEmail()) != null){
                throw new ExistRecordException("Account with such Email already exist");
            }
            if (accountDao.findByLogin(account.getLogin()) != null){
                throw new ExistRecordException("Account with such Login already exist");
            }
            return  accountDao.addAccount(account);
        } catch ( SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int addRoles(Long accountId, List<Role> roles) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            return accountRoleDao.addRolesForAccount(accountId, roles);
        } catch ( SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int updateRoles(Long accountId, List<Role> roles) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            return accountRoleDao.updateRolesForAccount(accountId, roles);
        } catch ( SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int deleteRoles(Long accountId, List<Role> roles) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            return accountRoleDao.deleteRolesForAccount(accountId, roles);
        } catch ( SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int addToken(Long idAccount, String token) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            return accountRegistrationDao.addToken(idAccount, token);
        } catch ( SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public void accountConfirmation(String token) {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
             Long id = accountRegistrationDao.getIdAccountByToken(token);
             if (id != null){
                 setStatusById(id,true );
             }
        } catch ( SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }
}
