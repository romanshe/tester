package it.source.tester.Services.impl;

import it.source.tester.Services.*;
import it.source.tester.Util.Util;
import it.source.tester.dao.*;
import it.source.tester.dao.impl.*;
import org.apache.commons.dbcp2.BasicDataSource;

import javax.servlet.ServletContext;
import java.sql.SQLException;
import java.util.Properties;

public class ServiceManager {
    private static final String SERVICE_MANAGER = "SERVICE_MANAGER";

    private final BasicDataSource dataSource;
    private final AccountDao accountDao;
    private final TestDao testDao;
    private final QuestionDao questionDao;
    private final AnswerDao answerDao;
    private final CommonService commonService;
    private final AdminService adminService;
    private final UserService userService;
    private final TutorService tutorService;
    private final RoleDao roleDao;
    private final ResultDao resultDao;
    private final AccountRoleDao accountRoleDao;
    private final I18nService i18nService;
    private final Properties applicationProperties = new Properties();
    private final NotificationService notificationService;
    private final AccountRegistrationDao accountRegistrationDao;

    public ServiceManager() {
        Util.loadProperties(applicationProperties, "application.properties");
        this.dataSource = setupDataSource();
        this.accountDao = new AccountDaoImpl();
        this.testDao = new TestDaoImpl();
        this.questionDao = new QuestionDaoImpl();
        this.answerDao = new AnswerDaoImpl();
        this.roleDao = new RoleDaoImpl();
        this.resultDao = new ResultDaoImpl();
        this.accountRoleDao = new AccountRoleDaoImpl();
        this.accountRegistrationDao = new AccountRegistrationDaoImpl();
        this.commonService = new CommonServiceImpl(dataSource, accountDao, roleDao);
        this.adminService = new AdminServiceImpl(dataSource, accountDao, roleDao, accountRoleDao, accountRegistrationDao);
        this.userService = new UserServiceImpl(dataSource, testDao, questionDao, answerDao, resultDao);
        this.tutorService = new TutorServiceImpl(dataSource, testDao, questionDao, answerDao);
        this.i18nService = new I18nServiceImpl();
        this.notificationService = new AsyncEmailNotificationService(this);
    }

    public static ServiceManager getInstance(ServletContext sc) {
        ServiceManager serviceManager = (ServiceManager) sc.getAttribute(SERVICE_MANAGER);
        if (serviceManager == null) {
            serviceManager = new ServiceManager();
            sc.setAttribute(SERVICE_MANAGER, serviceManager);
        }
        return serviceManager;
    }
    public String getApplicationProperty(String key)
    {
        String value = applicationProperties.getProperty(key);
//        if (value.startsWith("${sysEnv."))
//        {
//            String keyVal = value.replace("${sysEnv.", "").replace("}", "");
//            value = System.getProperty(keyVal);
//        }
        return value;
    }

    private BasicDataSource setupDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        //dataSource.setDefaultAutoCommit(false);
        //dataSource.setRollbackOnReturn(true);
        dataSource.setDriverClassName(getApplicationProperty("db.driver"));
        //dataSource.setUrl("jdbc:postgresql://localhost:5432/postgres");
        //dataSource.setUsername("postgres");

        //dataSource.setUrl("jdbc:postgresql://localhost:5432/test");
        //dataSource.setUsername("test");
        //
        dataSource.setUrl(getApplicationProperty("db.url"));
        dataSource.setUsername(getApplicationProperty("db.username"));
        dataSource.setPassword(getApplicationProperty("db.password"));
        dataSource.setInitialSize(Integer.parseInt(getApplicationProperty("db.pool.initSize")));
        dataSource.setMaxTotal(Integer.parseInt(getApplicationProperty("db.pool.maxSize")));
        return dataSource;
    }

    public void shutdown() {
        try {
            dataSource.close();
        } catch (SQLException e) {
            //logger
        }
    }

    public CommonService getCommonService(){
        return commonService;
    }

    public AdminService getAdminService(){
        return adminService;
    }

    public UserService getUserService(){
        return userService;
    }

    public TutorService getTutorService() {
        return tutorService;
    }

    public ResultDao getResultDao() {
        return resultDao;
    }

    public I18nService getI18nService() {
        return i18nService;
    }

    public NotificationService getNotificationService() {
        return notificationService;
    }
}
