package it.source.tester.Services.impl;

import it.source.tester.Services.AdvancedTutorService;
import it.source.tester.dao.AnswerDao;
import it.source.tester.dao.QuestionDao;
import it.source.tester.dao.TestDao;
import it.source.tester.entity.Test;
import it.source.tester.exeption.ValidationException;

import javax.sql.DataSource;
import java.util.List;

public class AdvancedTutorServiceImpl extends TutorServiceImpl implements AdvancedTutorService {

    public AdvancedTutorServiceImpl(DataSource dataSource, TestDao testDao, QuestionDao questionDao, AnswerDao answerDao) {
        super(dataSource, testDao, questionDao, answerDao);
    }


    @Override
    public List<Test> getTestList() throws ValidationException {
        return super.getTestList();
    }
}
