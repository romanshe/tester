package it.source.tester.Services.impl;

import it.source.tester.Services.CommonService;
import it.source.tester.dao.AccountDao;
import it.source.tester.dao.RoleDao;
import it.source.tester.entity.Account;
import it.source.tester.exeption.ApplicationException;
import it.source.tester.exeption.ValidationException;
import it.source.tester.factory.ConnectionUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class CommonServiceImpl implements CommonService {

    private final AccountDao accountDao;
    private final DataSource dataSource;
    private final RoleDao roleDao;

    public CommonServiceImpl(DataSource dataSource, AccountDao accountDao, RoleDao roleDao) {
        this.accountDao = accountDao;
        this.dataSource = dataSource;
        this.roleDao = roleDao;
    }


    @Override
    public Account login(String login, String password) throws ValidationException {
        try {
            Connection connection = dataSource.getConnection();
            ConnectionUtils.setCurrentConnection(connection);
            Account account = accountDao.findByLogin(login);
            if (account == null) {
                throw new ValidationException("Account with such login does not exist " + login);
            }
            if (!account.getPassword().equals(password)) {
                throw new ValidationException("Password mismatch");
            }
            account.setRoles(roleDao.findRoleById(account.getId()));
            return account;
        } catch (SQLException e) {
            throw new ApplicationException(e);
        }
    }
}
