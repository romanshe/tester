package it.source.tester.Services.impl;

import it.source.tester.Services.UserService;
import it.source.tester.dao.AnswerDao;
import it.source.tester.dao.QuestionDao;
import it.source.tester.dao.ResultDao;
import it.source.tester.dao.TestDao;
import it.source.tester.entity.*;
import it.source.tester.exeption.ApplicationException;
import it.source.tester.exeption.ValidationException;
import it.source.tester.factory.ConnectionUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class UserServiceImpl implements UserService {

    protected final DataSource dataSource;
    protected final AnswerDao answerDao;
    protected final TestDao testDao;
    protected final QuestionDao questionDao;
    protected final ResultDao resultDao;

    public UserServiceImpl(DataSource dataSource, TestDao testDao, QuestionDao questionDao, AnswerDao answerDao, ResultDao resultDao) {
        this.dataSource = dataSource;
        this.testDao = testDao;
        this.questionDao = questionDao;
        this.answerDao = answerDao;
        this.resultDao = resultDao;
    }

    @Override
    public Test getTest(long idTest) throws ValidationException {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            Test test = testDao.findById(idTest);
            if (test == null) {
                throw new ValidationException("No test was found ");
            }
            return test;
        } catch (SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public List<Test> getTestList() throws ValidationException {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            List<Test> testList = testDao.findAll();
            if (testList == null) {
                throw new ValidationException("No test was found ");
            }
            return testList;
        } catch (SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public List<Question> getQuestionList(long idTest) throws ValidationException {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            List<Question> questionList = questionDao.findByIdTest(idTest);
            if (questionList == null) {
                throw new ValidationException("Question wasn't been found ");
            }
            for (Question question : questionList) {
                question.setAnswers(answerDao.findByIdQuestion(question.getId()));
            }
            return questionList;
        } catch (SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public List<Answer> getAnswerForQuestion(long idQuestion) throws ValidationException {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            List<Answer> answerList =  answerDao.findByIdQuestion(idQuestion);
            if (answerList == null) {
                throw new ValidationException("Answer wasn't been found ");
            }
            return answerList;
        } catch ( SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public int addResult(Result result) throws ValidationException {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            int  result1 =  resultDao.addResult(result);
            if (result1 == 0) {
                throw new ValidationException("Result wasn't been added");
            }
            return result1;
        } catch ( SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public List<Result> getResultList(long idAccount) throws ValidationException {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            List<Result>  result =  resultDao.findByIdAccount(idAccount);
            if (result.size() == 0) {
                throw new ValidationException("Result wasn't been find");
            }
            return result;
        } catch ( SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }

    @Override
    public List<Test> getTestList(long accountId) throws ValidationException {
        try (Connection connection = dataSource.getConnection()){
            ConnectionUtils.setCurrentConnection(connection);
            List<Test> testList = testDao.findByAccountId(accountId);
            if (testList == null) {
                throw new ValidationException("No test was found ");
            }
            return testList;
        } catch (SQLException e) {
            throw new ApplicationException(e);
        } finally {
            ConnectionUtils.clearCurrentConnection();
        }
    }
}
