package it.source.tester.Services;

import it.source.tester.entity.*;
import it.source.tester.exeption.ValidationException;

import java.util.List;

public interface UserService {

    List<Test> getTestList( ) throws ValidationException;
    List<Test> getTestList(long accountId ) throws ValidationException;
    Test getTest(long idTest ) throws ValidationException;
    List<Question> getQuestionList(long idTest ) throws ValidationException;
    List<Answer> getAnswerForQuestion(long idQuestion) throws ValidationException;
    int addResult(Result result) throws ValidationException;
    List<Result> getResultList(long idAccount) throws ValidationException;
}
