package it.source.tester.Services;

import it.source.tester.entity.Test;
import it.source.tester.exeption.ValidationException;

import java.util.List;

public interface AdvancedTutorService extends TutorService {

    @Override
    List<Test> getTestList( ) throws ValidationException;

}
