package it.source.tester.Services;

import it.source.tester.entity.Answer;
import it.source.tester.entity.Question;
import it.source.tester.entity.Test;
import it.source.tester.exeption.ExistRecordException;
import it.source.tester.exeption.ValidationException;

public interface TutorService extends UserService {

    int addTest(Test test) throws ExistRecordException;
    int updateTest(Test test);
    int removeTest(Long idTest);


    int addQuestion(Question question);
    int updateQuestion(Question question);
    int removeQuestion(Long idQuestion);
    Question getQuestionById(long idQuestion) throws ValidationException;

    int addAnswer(Answer answer);
    int updateAnswer(Answer answer);
    int removeAnswer(Long idAnswer);
    Answer getAnswerById(long idAnswer) throws ValidationException;



}
