package it.source.tester.filter;

import it.source.tester.entity.Account;
import it.source.tester.entity.Role;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static it.source.tester.Constants.*;

@WebFilter(filterName = "AuthenticationFilter")
public class AuthenticationFilter extends AbstractFilter {
    @Override
    public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain) throws IOException, ServletException {


        Long roleId = (Long) req.getSession().getAttribute(ATTRIBUTE_CURRENT_ROLE_ID);
        String currentUri = req.getRequestURI();


        if (currentUri.endsWith("/login") || currentUri.startsWith(APP_CONTEXT_PATH + "/static") || currentUri.endsWith("/sign-up")) {
            chain.doFilter(req, resp);
        } else if (roleId==null) {
            resp.sendRedirect(APP_CONTEXT_PATH + "/login");
        } else {
            if (currentUri.endsWith("/logout") || currentUri.endsWith("/my-info")){
                chain.doFilter(req, resp);
            } else if (currentUri.startsWith(APP_CONTEXT_PATH + "/tutor")){
                if ((roleId == TUTOR_ROLE_ID) || (roleId == ADVANCED_TUTOR_ROLE_ID)){
                    chain.doFilter(req, resp);
                } else {
                    resp.sendRedirect(APP_CONTEXT_PATH + "/login");
                }
            } else if (currentUri.startsWith(APP_CONTEXT_PATH + "/student")) {
                if ((roleId == STUDENT_ROLE_ID) || (roleId == TUTOR_ROLE_ID) || (roleId == ADVANCED_TUTOR_ROLE_ID)){
                    chain.doFilter(req, resp);
                } else {
                    resp.sendRedirect(APP_CONTEXT_PATH + "/login");
                }
            }else if (currentUri.startsWith(APP_CONTEXT_PATH + "/admin")) {
                if ((roleId == ADMIN_ROLE_ID)){
                    chain.doFilter(req, resp);
                } else {
                    resp.sendRedirect(APP_CONTEXT_PATH + "/login");
                }
            }
        }
    }

}
