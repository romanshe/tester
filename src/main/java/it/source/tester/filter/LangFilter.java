package it.source.tester.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

import static it.source.tester.Constants.PARAMETER_LANG;
import static it.source.tester.Constants.SESSION_LOCALE;


@WebFilter(filterName = "LangFilter")
public class LangFilter extends AbstractFilter
{
	protected final Logger LOG = LoggerFactory.getLogger(getClass());

	@Override
	public void doFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain) throws IOException, ServletException
	{

		String lang = req.getParameter(PARAMETER_LANG);
		if (lang != null) {
			req.getSession().setAttribute(SESSION_LOCALE, new Locale(lang));
		}

		try
		{
			HttpServletRequest langReq = new HttpServletRequestWrapper(req){
				@Override
				public Locale getLocale()
				{
					return (Locale) req.getSession().getAttribute(SESSION_LOCALE);
				}
			};

			chain.doFilter(langReq, resp);
		}
		catch (Throwable th)
		{
			LOG.error("Error during request: " + req.getRequestURI(), th);
			//resp.sendRedirect("/error");
		}

	}
}
