package it.source.tester.handler;

import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DefaultOneRowResultSetHandler<T> implements ResultSetHandler<T> {
    private ResultSetHandler<T> oneRowResultSetHandler;

    public DefaultOneRowResultSetHandler(ResultSetHandler<T> oneRowResultSetHandler) {
        this.oneRowResultSetHandler = oneRowResultSetHandler;
    }

    public T handle(ResultSet resultSet) throws SQLException {
        if (resultSet.next()) {
            return oneRowResultSetHandler.handle(resultSet);
        } else {
            return null;
        }
    }
}
