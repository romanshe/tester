package it.source.tester.handler;

import org.apache.commons.dbutils.ResultSetHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

public class DefaultListResultSetHandler<T> implements ResultSetHandler<List<T>> {
    private ResultSetHandler<T> oneRowResultSetHandler;

    public DefaultListResultSetHandler(ResultSetHandler<T> oneRowResultSetHandler) {
        this.oneRowResultSetHandler = oneRowResultSetHandler;
    }

    public List<T> handle(ResultSet resultSet) throws SQLException {
        List<T> result = new ArrayList();

        while (resultSet.next()){
            result.add(oneRowResultSetHandler.handle(resultSet));
        }
        if (result.size() > 0) {
            return result;
        } else {
            return null;
        }
    }
}
