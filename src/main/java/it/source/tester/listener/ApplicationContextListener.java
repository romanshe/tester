package it.source.tester.listener;


import it.source.tester.Services.impl.ServiceManager;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;


@WebListener
public class ApplicationContextListener implements ServletContextListener {


    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("Context listener init");
        ServiceManager.getInstance(servletContextEvent.getServletContext());
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("Context listener destroy");
        ServiceManager.getInstance(servletContextEvent.getServletContext()).shutdown();
    }
}
