package it.source.tester.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Locale;

import static it.source.tester.Constants.SESSION_LOCALE;


@WebListener
public class ApplicationSessionListener implements HttpSessionListener {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        LOGGER.info("Created session: {}", httpSessionEvent.getSession().getId());
        httpSessionEvent.getSession().setAttribute(SESSION_LOCALE, new Locale("ru"));


    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        LOGGER.info("Destroyed session: {}", httpSessionEvent.getSession().getId());

    }
}
