package it.source.tester.tag;

import it.source.tester.Services.impl.ServiceManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.Locale;

import static it.source.tester.Constants.SESSION_LOCALE;


public class ResourceBundleReaderTag extends SimpleTagSupport
{

	private String key;

	public void setKey(String key) {
		this.key = key;
	}
	@Override
	public void doTag() throws JspException, IOException
	{
		JspWriter out = getJspContext().getOut();
		PageContext pageContext = (PageContext) getJspContext();
		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
		ServiceManager serviceManager = ServiceManager.getInstance(request.getServletContext());
		Locale sessionLocale = (Locale) request.getSession().getAttribute(SESSION_LOCALE);
		String value = serviceManager.getI18nService().getMessage(key, sessionLocale);
		out.print(value);
	}
}
